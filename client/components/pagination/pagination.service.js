'use strict';

angular.module('sdoApp')
  .factory('PaginationService', [function() {
    return {
      getPageFromHash: function ($location) {
        var hash = $location.hash()
        if(!hash) { return 1; }
        if(hash.search(/^[p][0-9]*$/) == 0){
          return hash.substring(1)
        } else {
          return 1
        }
      },
      setHashFromPage: function ($location, number) {
        if(number == 1) {
          $location.hash('')
        } else {
          $location.hash('p'+number)
        }
      }
    }
}]);
