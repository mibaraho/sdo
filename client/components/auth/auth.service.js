'use strict';

(function() {

function AuthService($location, $http, $cookies, $state, $q, appConfig, Util, User) {
  var safeCb = Util.safeCb;
  var currentUser = {};
  var userRoles = appConfig.userRoles || [];

  if ($cookies.get('token') && $location.path() !== '/logout') {
    currentUser = User.get();
  }

  var Auth = {

    /**
     * Authenticate user and save token
     *
     * @param  {Object}   user     - login info
     * @param  {Function} callback - optional, function(error, user)
     * @return {Promise}
     */
    login({email, password}, callback) {
      return $http.post('/auth/local', {
        email: email,
        password: password
      })
        .then(res => {
          $cookies.put('token', res.data.token);
          currentUser = User.get();
          return currentUser.$promise;
        })
        .then(user => {
          $cookies.put('current_merchant_id', user.Merchants[0]._id);
          $cookies.put('current_merchant_name', user.Merchants[0].name);
          safeCb(callback)(null, user);
          return user;
        })
        .catch(err => {
          safeCb(callback)(err.data);
          return $q.reject(err.data);
        });
    },

    /**
     * Delete access token and user info
     */
    logout() {

      $cookies.remove('token');
      $cookies.remove('current_merchant_id');
      $cookies.remove('current_merchant_name');
      currentUser = {};
      $state.go('main')
    },

    /**
     * Create a new user
     *
     * @param  {Object}   user     - user info
     * @param  {Function} callback - optional, function(error, user)
     * @return {Promise}
     */
    createUser(user, callback) {
      return User.save(user,
        function(data) {
          $cookies.put('token', data.token);
          $cookies.put('current_merchant_id', data.merchant._id);
          $cookies.put('current_merchant_name', data.merchant.name);
          currentUser = User.get();
          return safeCb(callback)(null, user);
        },
        function(err) {
          return safeCb(callback)(err);
        }).$promise;
    },

    /**
     * Change password
     *
     * @param  {String}   oldPassword
     * @param  {String}   newPassword
     * @param  {Function} callback    - optional, function(error, user)
     * @return {Promise}
     */
    changePassword(oldPassword, newPassword, callback) {
      return User.changePassword({ id: currentUser._id }, {
        oldPassword: oldPassword,
        newPassword: newPassword
      }, function() {
        return safeCb(callback)(null);
      }, function(err) {
        return safeCb(callback)(err);
      }).$promise;
    },

    /**
     * Gets all available info on a user
     *   (synchronous|asynchronous)
     *
     * @param  {Function|*} callback - optional, funciton(user)
     * @return {Object|Promise}
     */
    getCurrentUser(callback) {
      if (arguments.length === 0) {
        currentUser.currentMerchant = {
            _id: $cookies.get('current_merchant_id'),
            name: $cookies.get('current_merchant_name')
          }
        return currentUser;
      }
      var value = (currentUser.hasOwnProperty('$promise')) ?
        currentUser.$promise : currentUser;
      return $q.when(value)
        .then(user => {
          user.currentMerchant = {
            _id: $cookies.get('current_merchant_id'),
            name: $cookies.get('current_merchant_name')
          }
          safeCb(callback)(user);
          return user;
        }, () => {
          safeCb(callback)({});
          return {};
        });
    },

    getCurrentMerchant() {
      return {
            _id: $cookies.get('current_merchant_id'),
            name: $cookies.get('current_merchant_name')
        }
    },

    /**
     * Check if a user is logged in
     *   (synchronous|asynchronous)
     *
     * @param  {Function|*} callback - optional, function(is)
     * @return {Bool|Promise}
     */
    isLoggedIn(callback) {
      if (arguments.length === 0) {
        return currentUser.hasOwnProperty('role');
      }

      return Auth.getCurrentUser(null)
        .then(user => {
          var is = user.hasOwnProperty('role');
          safeCb(callback)(is);
          return is;
        });
    },

     /**
      * Check if a user has a specified role or higher
      *   (synchronous|asynchronous)
      *
      * @param  {String}     role     - the role to check against
      * @param  {Function|*} callback - optional, function(has)
      * @return {Bool|Promise}
      */
    hasRole(role, callback) {
      var hasRole = function(r, h) {
        return userRoles.indexOf(r) >= userRoles.indexOf(h);
      };

      if (arguments.length < 2) {
        return hasRole(currentUser.role, role);
      }

      return Auth.getCurrentUser(null)
        .then(user => {
          var has = (user.hasOwnProperty('role')) ?
            hasRole(user.role, role) : false;
          safeCb(callback)(has);
          return has;
        });
    },

     /**
      * Check if a user is an admin
      *   (synchronous|asynchronous)
      *
      * @param  {Function|*} callback - optional, function(is)
      * @return {Bool|Promise}
      */
    isAdmin() {
      return Auth.hasRole
        .apply(Auth, [].concat.apply(['admin'], arguments));
    },

    /**
     * Get auth token
     *
     * @return {String} - a token string used for authenticating
     */
    getToken() {
      return $cookies.get('token');
    },

    /**
     * Set current merchant
     *
     * @return void
     */
    setCurrentMerchant(merchant) {
      $cookies.put('current_merchant_id', merchant._id);
      $cookies.put('current_merchant_name', merchant.name);
    },
    /**
     * Get current merchant
     *
     * @return merchant
     */
    getCurrentMerchant(merchant) {
      return {
            _id: $cookies.get('current_merchant_id'),
            name: $cookies.get('current_merchant_name')
        }
    }
  };

  return Auth;
}

angular.module('sdoApp.auth')
  .factory('Auth', AuthService);

})();
