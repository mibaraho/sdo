'use strict';

angular.module('sdoApp.auth', [
  'sdoApp.constants',
  'sdoApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
