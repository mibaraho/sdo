'use strict';

angular.module('sdoApp')
  .factory('MercadolibreQuestionsService', ['$resource', 'Auth', function($resource, Auth) {
    return $resource('/api/mercadolibre-connections/:id', null,
      {
        questions: {
          url: '/api/mercadolibre-connections/:id/products/:itemId/questions',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        answer: {
          url: '/api/mercadolibre-connections/:id/questions/:questionId/answer',
          method:'POST',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        }
      });
}]);
