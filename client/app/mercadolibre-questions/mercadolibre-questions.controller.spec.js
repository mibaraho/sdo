'use strict';

describe('Controller: MercadolibreQuestionsCtrl', function () {

  // load the controller's module
  beforeEach(module('sdoApp'));

  var MercadolibreQuestionsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MercadolibreQuestionsCtrl = $controller('MercadolibreQuestionsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
