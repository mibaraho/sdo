'use strict';

angular.module('sdoApp')
  .controller('MercadolibreQuestionsModalCtrl', function ($scope, $uibModalInstance, questions, mercadolibreConnection, MercadolibreQuestionsService, WatsonService, Auth) {

    $scope.questions = questions
    $scope.action = 'create';
    $scope.brand = {}
    $scope.currentUser = {}
    $scope.disabled = true;
    var thresholds = {
      anger: 0.5,
      sadness: 0.5,
      openness: 0.1,
      agreeableness: 0.1,
      extraversion: 0.1
    }


    _.each($scope.questions, function(item){
      item.isOpened = !item.answer
      if(!item.answer){
        item.findingSuggestions = true;
        WatsonService.suggestAnswer({}, {question: item.text}, function(result){
          item.new_answer = result.output.text
          
          if(item.new_answer=='NO_SUGGESTION_FOUND'){
            item.new_answer = '';
          }else{
            item.new_answer = String(item.new_answer);
          }
          console.log(item.new_answer)
          item.findingSuggestions = false;
        }, function(err){
          item.findingSuggestions = false;
          console.log(err)
        })
      }
    })

    $scope.answer = function(question){
      
      var answer = {
        text: question.new_answer
      }
      return MercadolibreQuestionsService.answer({ id: mercadolibreConnection._id, questionId: question.id }, answer, function(result){
          question.answer = result.answer
        }, function(err){
          console.log(err)
        })
    }
    $scope.validate = function(question){
      var answer = {
        text: question.new_answer
      }
      console.log(answer)
      return WatsonService.validateAnswer({}, answer, function(result){
          question.answer_feedback = {}
          question.answer_feedback.raw = result

          question.new_answer = question.new_answer || {}

          var emotion_tone = _.find(question.answer_feedback.raw.document_tone.tone_categories, function(item){return item.category_id == 'emotion_tone'})
          var social_tone = _.find(question.answer_feedback.raw.document_tone.tone_categories, function(item){return item.category_id == 'social_tone'})

          var anger = _.find(emotion_tone.tones, function(item){ return item.tone_id == 'anger'})
          var sadness = _.find(emotion_tone.tones, function(item){ return item.tone_id == 'sadness'})
          var openness = _.find(social_tone.tones, function(item){ return item.tone_id == 'openness_big5'})
          var agreeableness = _.find(social_tone.tones, function(item){ return item.tone_id == 'agreeableness_big5'})
          var extraversion = _.find(social_tone.tones, function(item){ return item.tone_id == 'extraversion_big5'})


          question.answer_feedback.tooAngry = anger.score > thresholds.anger
          question.answer_feedback.tooSad =  sadness.score > thresholds.sadness
          question.answer_feedback.littleHappy =  extraversion.score < thresholds.extraversion
          question.answer_feedback.littleOpen = openness.score < thresholds.openness
          question.answer_feedback.littleAgreeableness = agreeableness.score < thresholds.agreeableness
          console.log(result)
        }, function(err){
          console.log(err)
        })

    }

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    Auth.getCurrentUser(function(currentUser){
      $scope.currentUser = currentUser;
      $scope.disabled = false;
    })

  });
