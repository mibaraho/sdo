'use strict';

angular.module('sdoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mercadolibre-questions', {
        url: '/mercadolibre-questions',
        templateUrl: 'app/mercadolibre-questions/mercadolibre-questions.html',
        controller: 'MercadolibreQuestionsCtrl'
      });
  });
