'use strict';

angular.module('sdoApp').directive('sdoMercadolibreProductsTileDirective', function(MercadolibreConnectionsService, $uibModal, Auth) {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      itemId: '=itemid',
      marketplaceConnectionId: '=marketplaceconnectionid'
    },
    link: function($scope, element, attrs) {
            Auth.getCurrentUser(function(currentUser){
            $scope.currentUser = currentUser;
            initialize()
          })
      var initialize = function(){
          $scope.pendingActions = true;
          $scope.err = false;
          var getProduct = function() {
              $scope.pendingActions = true;
              MercadolibreConnectionsService.product({ id: $scope.marketplaceConnectionId, itemId : $scope.itemId }).$promise.then(function(result){
                $scope.item = result
                MercadolibreConnectionsService.questions({ id: $scope.marketplaceConnectionId, itemId : $scope.itemId }).$promise.then(function(result){
                  $scope.item.questions = {}
                  $scope.item.questions.answered = _.filter(result.questions, function(item){
                    return item.status == "ANSWERED"
                  })
                  $scope.item.questions.unanswered = _.filter(result.questions, function(item){
                    return item.status == "UNANSWERED"
                  })
                  $scope.item.questions.all = result.questions
                  $scope.pendingActions = false;
                }, function(err){
                  $scope.error = true;
                  $scope.pendingActions = false;
                });
              }, function(err){
                $scope.error = true;
                $scope.pendingActions = false;
              });
          }
          getProduct();
          $scope.showAllQuestions = function(){
            $scope.openQuestionsModal('all')
          }
          $scope.showAnsweredQuestions = function(){
            $scope.openQuestionsModal('answered')
          }
          $scope.showUnansweredQuestions = function(){
            $scope.openQuestionsModal('unanswered')
          }

        //Open questions modal
        $scope.openQuestionsModal = function (context) {
          $scope.displayQuestions = $scope.item.questions.all
          if(context == 'answered'){
            $scope.displayQuestions = $scope.item.questions.answered
          } else if (context == 'unanswered'){
            $scope.displayQuestions = $scope.item.questions.unanswered
          }
          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/mercadolibre-questions/mercadolibre-questions.modal.html',
            controller: 'MercadolibreQuestionsModalCtrl',
            resolve: {
               context: function () {
                 return context;
               },
               questions: function(){
                 return $scope.displayQuestions
               },
               mercadolibreConnection: function(){
                  return {
                    _id: $scope.marketplaceConnectionId
                  }
               }
             },
            size: 'md'
          });

          modalInstance.result.then(function (result) {
            $scope.brands.push(result)
            $scope.product.BrandId = result._id
          });
        };
        //End Create brand modal
      }
    },
    templateUrl: 'app/mercadolibre-products/mercadolibre-products.tile.directive.html'
  };
});
