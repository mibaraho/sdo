'use strict';

angular.module('sdoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mercadolibre-products', {
        url: '/mercadolibre-products',
        templateUrl: 'app/mercadolibre-products/mercadolibre-products.html',
        controller: 'MercadolibreProductsCtrl'
      });
  });
