'use strict';

describe('Controller: MercadolibreProductsCtrl', function () {

  // load the controller's module
  beforeEach(module('sdoApp'));

  var MercadolibreProductsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MercadolibreProductsCtrl = $controller('MercadolibreProductsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
