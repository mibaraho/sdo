'use strict';

angular.module('sdoApp.admin', [
  'sdoApp.auth',
  'ui.router'
]);
