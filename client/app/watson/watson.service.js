'use strict';

angular.module('sdoApp')
  .factory('WatsonService', ['$resource', 'Auth', function($resource, Auth) {
    return $resource('/api/mercadolibre-connections/:id', null,
      {
        validateAnswer: {
          url: '/api/auto-responses/translate-and-analyze',
          method:'POST',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        suggestAnswer: {
          url: '/api/auto-responses/ask',
          method:'POST',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        }
      });
}]);
