'use strict';

angular.module('sdoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mercadolibre-questions-dashboard', {
        url: '/mercadolibre-connections/:id/questions-dashboard',
        templateUrl: 'app/mercadolibre-questions-dashboard/mercadolibre-questions-dashboard.html',
        controller: 'MercadolibreQuestionsDashboardCtrl'
      });
  });
