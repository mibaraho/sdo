'use strict';

describe('Controller: MercadolibreQuestionsDashboardCtrl', function () {

  // load the controller's module
  beforeEach(module('sdoApp'));

  var MercadolibreQuestionsDashboardCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MercadolibreQuestionsDashboardCtrl = $controller('MercadolibreQuestionsDashboardCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
