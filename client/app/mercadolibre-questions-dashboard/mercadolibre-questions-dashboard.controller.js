'use strict';

angular.module('sdoApp')
  .controller('MercadolibreQuestionsDashboardCtrl', function ($scope, $stateParams, $location, MercadolibreConnectionsService, PaginationService, Auth) {

    $scope.pendingActions = true;
    $scope.err = false;
    $scope.marketplaceConnection = {
      _id: $stateParams.id
    }
    var pageFromHash = PaginationService.getPageFromHash($location)

    var getItems = function(page) {
      $scope.pendingActions = true;
      return MercadolibreConnectionsService.products({ id : $stateParams.id, page: page}).$promise.then(function(result){
        $scope.products = result.results;
        PaginationService.setHashFromPage($location, page)
        $scope.pendingActions = false;
      }, function(err){

        $scope.error = true;
        $scope.pendingActions = false;

      });
    }

    Auth.getCurrentUser(function(currentUser){
      $scope.currentUser = currentUser;
      getItems(pageFromHash)
    })

    $scope.pageChanged = function(){
      getItems($scope.pagination.current_page);
    }
  });
