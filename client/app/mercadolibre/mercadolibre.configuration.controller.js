'use strict';

angular.module('sdoApp')
  .controller('MercadolibreConfigurationCtrl', function ($scope, $q, $state, $stateParams, MercadolibreService, MercadolibreConnectionsService, StoresAndWarehousesService, MarketplaceConnectionsService, ProductPriceListsService, ProductLinksService, Auth) {

    $scope.pendingActions = true;
    $scope.err = false;

    var getItems = function(){
      var user = MercadolibreConnectionsService.user({ id: $stateParams.id });
      var configuration = MercadolibreConnectionsService.configuration({ id: $stateParams.id })
      var connection = MarketplaceConnectionsService.get({ id: $stateParams.id });
      $q.all([user.$promise, configuration.$promise, connection.$promise]).then(function(result){
        $scope.user = result[0];
        $scope.configuration = result[1]
        $scope.connection = result[2]
        $scope.goToBasicInfo()
        $scope.pendingActions = false;
      }, function(err){
        $scope.error = true;
        $scope.pendingActions = false;

      })
    }
    $scope.categoryTabSelected = function(){
      $scope.categoryMappingConfiguration = 'automatic'
    }
    $scope.categoryMappingConfigurationChanged = function(configuration) {
      $scope.categoryMappingConfiguration = configuration
    }
    var getProductsForSelection = function(page) {
      var _page = page || 1
      $scope.pendingActions = true;
      ProductLinksService.productsStatus({id: $scope.connection._id, page: _page}).$promise.then(function(result){
        $scope.items = result.entries
        _.each($scope.items, function(item){
          item.RepresentativeProductVersion = _.find(item.ProductVersions, function(_item){ return _item.ProductVersionPictures.length > 0}) || _.first(item.ProductVersions) //This is a representative product version. Filtering by the one who has pictures. This will be used to dislay pictures and to show the reference price
          item.RepresentativeProductPrice = _.first(item.RepresentativeProductVersion.ProductPrices)
          item.isLinked = (item.ProductLink && item.ProductLink.status == 'created') || false
        })
        $scope.pagination = result.pagination
        $scope.pendingActions = false;
      }, function(err){

        $scope.error = true;
        $scope.pendingActions = false;

      });
    }
    $scope.pageChanged = function(){
      getProductsForSelection($scope.pagination.current_page);
    }

    $scope.publishSettingsChanged = function(item){
      var productLink = {
        MarketplaceConnectionId: $scope.connection._id
      }
      if(item.isLinked){
        ProductLinksService.create({ id: item._id }, productLink, function(result){
        }, function(err){
          console.log(err)
        })
      } else {
        console.log('Now is not linked')
      }
    }
    $scope.productsTabSelected = function(){
      getProductsForSelection()
    }

    var getStockConfigurationItems = function() {
      $scope.stockUsagePolicyItemIds = []
      $scope.pendingActions = true;
      var stockUsagePolicyItems = MarketplaceConnectionsService.stockUsagePolicyItems({ id: $stateParams.id })
      var storesAndWarehouses = StoresAndWarehousesService.list({ id : $scope.currentUser.currentMerchant._id})
      $q.all([stockUsagePolicyItems.$promise, storesAndWarehouses.$promise]).then(function(result){
        $scope.stockUsagePolicyItems = result[0].entries
        $scope.connection.stockUsagePolicyItemWarehousesIds = _.map($scope.stockUsagePolicyItems, function(item) {return item.WarehouseId});
        $scope.storesAndWarehouses = result[1].entries
        $scope.pendingActions = false;
      }, function(err){

        $scope.error = true;
        $scope.pendingActions = false;

      });
    }
    $scope.stockUsageConfigurationTabSelected = function(){
      getStockConfigurationItems()
    }

    var getListingConfigurationItems = function() {
      $scope.pendingActions = true;
      var listingTypes = MercadolibreService.listingTypes({ country: $scope.connection.country })
      var conditions = MercadolibreService.conditions()
      $q.all([listingTypes.$promise, conditions.$promise]).then(function(result){
        $scope.listingTypes = result[0]
        $scope.conditions = result[1]
        $scope.pendingActions = false;
      }, function(err){

        $scope.error = true;
        $scope.pendingActions = false;

      });
    }

    $scope.listingConfigurationTabSelected = function(){
      getListingConfigurationItems()
    }


    var getPricingConfigurationItems = function() {
      $scope.pendingActions = true;
      var country = MercadolibreService.country({country: $scope.connection.country})
      $q.when()
        .then(function(){
          return country.$promise
        })
        .then(function(result){
          return ProductPriceListsService.list({ id : $scope.currentUser.currentMerchant._id, _currency_id_: result.currency_id }).$promise
        })
        .then(function(result){
          $scope.pricingLists = result.entries
          $scope.pendingActions = false;
        })
    }
    $scope.princingConfigurationTabSelected = function(){
      getPricingConfigurationItems()
    }

    $scope.finalStepTabSelected = function(){

    }

    $scope.startSynchronization = function(){
        $q.when()
          .then(function(result){
            return MercadolibreConnectionsService.updateConfiguration({ id: $stateParams.id }, $scope.configuration ).$promise
          })
          .then(function(result){
            return MarketplaceConnectionsService.updateStockUsagePolicy({ id: $stateParams.id }, $scope.connection.stockUsagePolicyItemWarehousesIds ).$promise
          })
          .then(function(result){
            MercadolibreConnectionsService.startSynchronization({ id: $stateParams.id }, {}, function(result){
              $state.go('mercadolibre-products.products', {id: $scope.connection._id})
            }, function(err){
              console.log(err)
            })
          })

    }
    Auth.getCurrentUser(function(currentUser){
      $scope.currentUser = currentUser;
      getItems()
    })

    //Go to's
    $scope.goToBasicInfo = function(){
      $scope.tabEnabled = 'basic-info'
      $scope.steps.step1 = true
      $scope.steps.step2 = false
      $scope.steps.step3 = false
      $scope.steps.step4 = false
      $scope.steps.step5 = false
      $scope.steps.step6 = false
      $scope.active = 0
    }

    $scope.goToProductsConfiguration = function(){
      $scope.tabEnabled = 'products-configuration'
      $scope.steps.step1 = false
      $scope.steps.step2 = true
      $scope.steps.step3 = false
      $scope.steps.step4 = false
      $scope.steps.step5 = false
      $scope.steps.step6 = false
      $scope.active = 1
    }

    $scope.goToPricingConfiguration = function(){
      $scope.tabEnabled = 'pricing-configuration'
      $scope.steps.step1 = false
      $scope.steps.step2 = false
      $scope.steps.step3 = true
      $scope.steps.step4 = false
      $scope.steps.step5 = false
      $scope.steps.step6 = false
      $scope.active = 2
    }

    $scope.goToStockConfiguration = function(){
      $scope.tabEnabled = 'stock-configuration'
      $scope.steps.step1 = false
      $scope.steps.step2 = false
      $scope.steps.step3 = false
      $scope.steps.step4 = true
      $scope.steps.step5 = false
      $scope.steps.step6 = false
      $scope.active = 3
    }

    $scope.goToListingConfiguration = function(){
      $scope.tabEnabled = 'listing-configuration'
      $scope.steps.step1 = false
      $scope.steps.step2 = false
      $scope.steps.step3 = false
      $scope.steps.step4 = false
      $scope.steps.step5 = true
      $scope.steps.step6 = false
      $scope.active = 4
    }


    $scope.goToFinalStep = function(){
      $scope.tabEnabled = 'final-step'
      $scope.steps.step1 = false
      $scope.steps.step2 = false
      $scope.steps.step3 = false
      $scope.steps.step4 = false
      $scope.steps.step5 = false
      $scope.steps.step6 = true
      $scope.active = 5
    }

    //En got to's


  });
