'use strict';

describe('Controller: MercadolibreCtrl', function () {

  // load the controller's module
  beforeEach(module('sdoApp'));

  var MercadolibreCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MercadolibreCtrl = $controller('MercadolibreCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).to.equal(1);
  });
});
