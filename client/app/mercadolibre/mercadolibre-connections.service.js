'use strict';

angular.module('sdoApp')
  .factory('MercadolibreConnectionsService', ['$resource', 'Auth', function($resource, Auth) {
    return $resource('/api/mercadolibre-connections/:id', null,
      {
        products: {
          url: '/api/mercadolibre-connections/:id/products',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        product: {
          url: '/api/mercadolibre-connections/:id/products/:itemId',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },        get: {
          url: '/api/mercadolibre-connections/:id',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        user: {
          url: '/api/mercadolibre-connections/:id/user',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        configuration: {
          url: '/api/mercadolibre-connections/:id/configuration',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        updateConfiguration: {
          url: '/api/mercadolibre-connections/:id/configuration',
          method:'PUT',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        startSynchronization: {
          url: '/api/mercadolibre-connections/:id/synchronization/start',
          method:'PUT',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        list: {
          url: '/api/m/:id/mercadolibre-connections',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        paginate: {
          url: '/api/m/:id/mercadolibre-connections/p/:page',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        update: {
          url: '/api/mercadolibre-connections/:id',
          method:'PUT',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        questions: {
          url: '/api/mercadolibre-connections/:id/products/:itemId/questions',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        remove: {
          url: '/api/mercadolibre-connections/:id',
          method:'DELETE',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        }
      });
}]);
