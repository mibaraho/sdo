'use strict';

angular.module('sdoApp')
  .factory('MercadolibreService', ['$resource', 'Auth', function($resource, Auth) {
    return $resource('/api/mercadolibre/:id', null,
      {
        configuration: {
          url: '/api/mercadolibre/configuration',
          method:'GET',
          isArray: true,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        getAuthToken: {
          url: '/api/m/:id/mercadolibre/:country/get-auth-token',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        listingTypes: {
          url: '/api/mercadolibre/:country/listing-types',
          method:'GET',
          isArray: true,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        country: {
          url: '/api/mercadolibre/countries/:country',
          method:'GET',
          isArray: false,
          headers: {
            Authorization: Auth.getToken()
          }
        },
        conditions: {
          url: '/api/mercadolibre/conditions',
          method:'GET',
          isArray: true,
          headers: {
            Authorization: Auth.getToken()
          }
        }
      });
}]);
