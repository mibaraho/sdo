'use strict';

angular.module('sdoApp')
  .controller('MercadolibreCreateCtrl', function ($scope, $sce, MercadolibreService) {
    $scope.action = 'create';
    $scope.countries = []
    $scope.connection = {}
    $scope.config = {}
    MercadolibreService.configuration().$promise.then(function(result){
      $scope.countries = result
      console.log(result)
      if($scope.countries.length){
        $scope.connection.countryCode = $scope.countries[0].code
        $scope.config = _.find($scope.countries, function(item){ return item.code == $scope.connection.countryCode})
      }
    })

    $scope.countryChanged = function(){
      $scope.config = _.find($scope.countries, function(item){ return item.code == $scope.connection.countryCode})
    }
  });
