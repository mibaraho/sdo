'use strict';

angular.module('sdoApp')
  .controller('MercadolibreCtrl', function ($scope, $location, MercadolibreConnectionsService, PaginationService,  Auth) {
    $scope.pendingActions = true;
    $scope.err = false;

    var pageFromHash = PaginationService.getPageFromHash($location)

    var getItems = function(page) {
      $scope.pendingActions = true;
      MercadolibreConnectionsService.paginate({ id : $scope.currentUser.currentMerchant._id, page: page}).$promise.then(function(result){
        $scope.connections = result.entries;
        $scope.pagination = result.pagination;
        PaginationService.setHashFromPage($location, page)
        $scope.pendingActions = false;
      }, function(err){

        $scope.error = true;
        $scope.pendingActions = false;

      });
    }

    Auth.getCurrentUser(function(currentUser){
      $scope.currentUser = currentUser;
      getItems(pageFromHash)
    })

    $scope.pageChanged = function(){
      getItems($scope.pagination.current_page);
    }

  });
