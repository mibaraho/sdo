'use strict';

angular.module('sdoApp')
  .controller('MercadolibreRedirectUriCtrl', function ($scope, $state, $location, $stateParams, MercadolibreService, Auth) {
    $scope.code = $location.search().code
    $scope.country = $stateParams.country
    var getAuthToken = function(){
      MercadolibreService.getAuthToken({ id : $scope.currentUser.currentMerchant._id, country: $scope.country, code: $scope.code }).$promise.then(function(result){
        $state.go('mercadolibre-questions-dashboard', { id: result._id })
      })
    }
    Auth.getCurrentUser(function(currentUser){
      $scope.currentUser = currentUser;
      getAuthToken()
    })


  });
