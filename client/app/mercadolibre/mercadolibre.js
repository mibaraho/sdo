'use strict';

angular.module('sdoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mercadolibre', {
        abstract: true,
        templateUrl: 'app/app/app.html'
      })
      .state('mercadolibre.list', {
        url: '/mercadolibre',
        templateUrl: 'app/mercadolibre/mercadolibre.html',
        controller: 'MercadolibreCtrl'
      })
      .state('mercadolibre.create', {
        url: '/mercadolibre/create',
        templateUrl: 'app/mercadolibre/mercadolibre.cu.html',
        controller: 'MercadolibreCreateCtrl'
      })
      .state('mercadolibre.redirect-uri', {
        url: '/mercadolibre/:country/redirect_uri',
        templateUrl: 'app/mercadolibre/mercadolibre.redirect-uri.html',
        controller: 'MercadolibreRedirectUriCtrl'
      });
  });
