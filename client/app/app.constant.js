(function(angular, undefined) {
'use strict';

angular.module('sdoApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);