'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // Sequelize connecton opions
  sequelize: {
    uri: 'mysql://root:@127.0.0.1:3306/sdo',
    options: {
      logging: false
    }
  },
  host: {
    protocol: 'http',
    domain: 'localhost',
    port: 9000
  },
  redis: {
    pkg: 'ioredis',
    host: '127.0.0.1',
    password: null,
    port: 6379,
    database:  0,
    tls: null
  },
  sendEmails: true,
  sendgrid:{
    api_key: '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'
  },
  // Seed database on startup
  seedDB: false,
  mercadolibre: {
    provider: 'mercadolibre',
    auth_type: 'oauth',
    countries: [{
      name: 'Chile',//Chile
      code: 'CL',
      site: 'MLC',
      appId: '6504146330792548',
      secretKey: '8RuWRPBe9QOpjQlCNJyFRznclHRseDbo',
      authBaseUrl: 'http://auth.mercadolibre.com',
      redirectUri: 'https://localhost:9000/mercadolibre/cl/redirect_uri',
      callbackUrl: 'https://localhost:9000/mercadolibre/cl/callback_url',
      apiBaseUrl: 'https://api.mercadolibre.com',
      pictureUrl: '/assets/images/nuevo-logo-mercado-libre.jpg'
    }]
  },
  watson :{
    conversation:{
      username: '94f6b14d-6108-4ded-be41-2ebd676f6571',
      password: 'CNrVx1424SAQ',
      version_date: '2016-07-01',
      workspace_id: 'ae2ba02f-bed2-4cf5-919d-1f72ac861753'
    }
  }

};
