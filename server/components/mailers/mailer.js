'use strict';


import config from '../../config/environment';
import nodemailer from 'nodemailer';
import sgTransport from 'nodemailer-sendgrid-transport';


// create reusable transporter object using sendgrid

var transporterOptions = {
    auth: {
        api_key: config.sendgrid.api_key
    }
  }
var transporter = nodemailer.createTransport(sgTransport(transporterOptions));

  var defaultMailOptions = {
      from: 'Multivende <hola@multivende.com>', // sender address
      subject: 'Multivende',
      html: '<p></p>'
  };

export function send(options, callback){
  var mailOptions = {};
  mailOptions.from = options.from || defaultMailOptions.from
  mailOptions.to = options.to
  mailOptions.subject = options.subject,
  mailOptions.html = options.html || defaultMailOptions.html
  mailOptions.attachments = options.attachments || []
  transporter.sendMail(mailOptions, function(error, info){
      if(error){
          return callback(error);
      }
      console.log('Message sent');
      callback(null)
  });
}

exports.EMAIL_CODES = {
  EMAIL_TO_STAFF_WHEN_LEAD_IS_GENERATED_FROM_OUTSIDE: 'EMAIL_TO_STAFF_WHEN_LEAD_IS_GENERATED_FROM_OUTSIDE',
  EMAIL_TO_STAFF_WHEN_CONTACT_IS_REQUESTED: 'EMAIL_TO_STAFF_WHEN_CONTACT_IS_REQUESTED'
}

