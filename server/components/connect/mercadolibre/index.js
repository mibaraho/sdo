'use strict'
var rp = require('request-promise');
import _ from 'lodash';
import config from '../../../config/environment';
import {Currency} from '../../../sqldb';
import {MarketplaceConnection} from '../../../sqldb';
import {MercadolibreConnectionConfiguration} from '../../../sqldb';
import {MarketplaceConnectionStockUsagePolicyItem} from '../../../sqldb';
import {PlatformCurrency} from '../../../sqldb';
import {Product} from '../../../sqldb';
import {ProductLink} from '../../../sqldb';
import {ProductLinkLogEntry} from '../../../sqldb';
import {ProductPicture} from '../../../sqldb';
import {ProductPrice} from '../../../sqldb';
import {ProductStock} from '../../../sqldb';
import {ProductVersion} from '../../../sqldb';
import {ProductVersionPicture} from '../../../sqldb';
import {User} from '../../../sqldb';
import {Warehouse} from '../../../sqldb';


exports.uploadProduct = function(mercadolibreConnection, productLink, user) {
    return MarketplaceConnection.find({
      where: {
        _id: mercadolibreConnection.MarketplaceConnectionId
      }
    })
    .then(initializeUploadProductBundle())
    .then(findUser(user))
    .then(findMercadolibreConnectionConfiguration())
    .then(getMercadolibreProduct(productLink))
    .then(updateProductLinkStatus('synchronizing'))
    .then(getProductAndProductVersions())
    .then(getRepresentativeProductPicture())
    .then(decideStockUsagePolicy())
    .then(calculateAvailableStock())
    .then(decideCategoryForProductListing())
    .then(getProductPriceAndCurrency())
    .then(populateProduct())
    .then(validateProduct())
    .then(uploadProduct())
    .then(saveProductUploadContent())
    .then(updateProductLinkStatus('synchronized'))
    .then(storeSuccessLogEntry())
    .then(function(bundle){
      return;
    })
    .catch(handleExpiredTokenError(arguments, uploadProduct))
    .catch(function(err){
      var logEntry = {}
      logEntry.type = ProductLinkLogEntry.TYPES.ERROR,
      logEntry.code = ProductLinkLogEntry.CODES.ERROR_UPLOADING_PRODUCT
      logEntry.content = err
      logEntry.MarketplaceConnectionId = mercadolibreConnection.MarketplaceConnectionId
      logEntry.ProductLinkId = productLink.ProductLinkId
      //We won't get the MerchantId this time, but for audit we can get it from the other items
      return ProductLinkLogEntry.create(logEntry).then(function(result){
        return bundle
      })
      return;
    })
}

function initializeUploadProductBundle(){
  return function(mercadolibreConnection){
    var bundle = {} //The bundle object will collect result while going through promises
    bundle.mercadolibreConnection = mercadolibreConnection;
    return bundle
  }
}
function findUser(user) {
  return function(bundle) {
    return User.find({
      where: {
        _id: user.UserId
      }
    })
    .then(function(result){
      bundle.user = result;
      return bundle
    })
  };
}
function findMercadolibreConnectionConfiguration() {
  return function(bundle) {
    return MercadolibreConnectionConfiguration.find({
      where: {
        MarketplaceConnectionId: bundle.mercadolibreConnection._id
      }
    })
    .then(function(result){
      bundle.mercadolibreConnectionConfiguration = result;
      return bundle
    })
  };
}
function getMercadolibreProduct(productLink) {
  return function(bundle) {
    return ProductLink.find({
      where: {
        _id: productLink.ProductLinkId
      }
    })
    .then(function(result){
      bundle.productLink = result;
      return bundle
    })
  };
}
function updateProductLinkStatus(status){
  return function(bundle){
   return bundle.productLink.update({
      synchronizationStatus: status
    }).then(function(result){
        bundle.productLink = result
        return bundle
    })
  }
}
function getProductAndProductVersions() {
  return function(bundle) {
    return Product.find({
      where: {
        _id: bundle.productLink.ProductId,
        status: 'created'
      },
      include: [{
              model: ProductPicture,
              attributes: ['_id', 'title', 'description', 'MerchantId', 'fileType', 'provider', 'status', 'storagePath', 'fileName', 'storageUrl', 'fileKey'],
              required: false,
            },{
              model: ProductVersion,
              where: {
                status: 'created'
              },
              include: [{
                model: ProductVersionPicture,
                where:{
                  status: 'created'
                },
                attributes: ['_id', 'title', 'description', 'MerchantId', 'ProductVersionId', 'fileType', 'provider', 'status', 'storagePath', 'fileName', 'storageUrl', 'fileKey'],
                required: false
              }]
      }]
    })
    .then(function(result){
      bundle.product = result;
      return bundle
    })
  };
}
function getRepresentativeProductPicture() {
  return function(bundle) {
    bundle.productRepresentativePictures = []
    if(bundle.product.ProductPictures.length){//If the product has pictures, the platform will use them
      bundle.productRepresentativePictures = _.map(bundle.product.ProductPictures, function(item){ return item.get().url})
    } else {//We look in the versions and get the first one we can find
      var _productVersionsPictures = _.map(bundle.product.ProductVersions, function(item){ return item.get().ProductVersionPictures })
      _productVersionsPictures = _.filter(_productVersionsPictures, function(item){ return item.length > 0 })
      bundle.productRepresentativePictures = _productVersionsPictures

    }
    return bundle
  };
}
function decideStockUsagePolicy() {
  return function(bundle) {
    return MarketplaceConnectionStockUsagePolicyItem.findAll({
      where: {
        status: 'created',
        MarketplaceConnectionId: bundle.mercadolibreConnection._id
      },
      include: [{
        model: Warehouse,
        attributes: ['_id', 'name'],
        where: {
          status: 'created'
        }
      }],
      order: [
        ['position']
      ]
    }).then(function(result){
      bundle.marketplaceConnectionStockUsagePolicyItems = result
      bundle.warehouses =_.map(bundle.marketplaceConnectionStockUsagePolicyItems, function(item){
        var _item = item.get();
          if(_item.Warehouse){
            return _item.Warehouse.get()
          } else  {
            return null
          }
        })
      return bundle
    })
  };
}
function calculateAvailableStock() {
  return function(bundle) {
    var _warehouses = _.map(bundle.warehouses, function(item){ return item._id })
    var _product_versions = _.map(bundle.product.get().ProductVersions, function(item){ return item._id })
    return ProductStock.findAll({
      where: {
        ProductVersionId: {
          $in: _product_versions
        },
        WarehouseId: {
          $in: _warehouses
        },
        status: 'created'
      }
    }).then(function(result){
      bundle.stock = _.sum(_.map(result, function(item){ return item.get().availableAmount }))
      return bundle
    })

  };
}

function decideCategoryForProductListing() {
  return function(bundle) {
      if(bundle.mercadolibreConnectionConfiguration.get().categoryMappingType == MercadolibreConnectionConfiguration.CategoryMappingTypes.AUTOMATIC) {
      var mercadolibre = config.mercadolibre
      var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == bundle.mercadolibreConnection.country.toLowerCase()})
      var options = {
          uri: _config.apiBaseUrl + '/sites/' + _config.site + '/category_predictor/predict',
          qs: {
                  title: bundle.product.name
              },
          json: true // Automatically parses the JSON string in the response
      };
      return rp(options)
        .then(function(result){
          bundle.category = result
          return bundle
        })
    } else {
      return bundle
    }
  };
}
function getProductPriceAndCurrency() {
  return function(bundle) {
    return ProductPrice.findAll({
      where: {
        ProductPriceListId: bundle.mercadolibreConnectionConfiguration.get().ProductPriceListId,
        status: 'created'
      },
      order: [['updatedAt', 'DESC']],
      include: [{
        model: ProductVersion,
        include: [{
          model: Product,
          where: {
            _id: bundle.product._id
          }
        }]
      },{
        model: Currency,
        include: [{
          model: PlatformCurrency
        }]
      }]
    })
    .then(function(result){
      if(result){
        var prices = _.map(result, function(item){ return item.get()}) //These are the prices for the product versions
        var maxPrice = _.maxBy(prices, function(o) { return o.gross; });//We select the max gross price among the versions
        bundle.productPrice = maxPrice.gross
        bundle.currency = maxPrice.Currency.PlatformCurrency.code
      } else {
        bundle.productPrice = 0
      }
      return bundle
    })
  };
}
function populateProduct() {
  return function(bundle) {
    var _product = {}
    _product.title = bundle.product.name
    _product.price = bundle.productPrice
    _product.currency_id = bundle.currency
    _product.available_quantity = bundle.stock
    _product.listing_type_id = bundle.mercadolibreConnectionConfiguration.ListingTypeId
    _product.condition = bundle.mercadolibreConnectionConfiguration.ProductConditionId
    _product.description = bundle.product.htmlDescription || bundle.product.description
    _product.category_id = bundle.category.id
    _product.pictures = []
    _.each(bundle.productRepresentativePictures, function(item){
      _product.pictures.push({
        source: item
      })
    })
    bundle._product = _product
    return bundle
  };
}
function validateProduct(user) {
  return function(bundle) {
    var mercadolibre = config.mercadolibre
    var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == bundle.mercadolibreConnection.country.toLowerCase()})
    var options = {
        method: 'POST',
        uri: _config.apiBaseUrl + '/items/validate',
        qs: {
                access_token: bundle.mercadolibreConnection.access_token
            },
        body: bundle._product,
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
      .then(function(result){
        return bundle
      })
  };
}
function uploadProduct(user) {
  return function(bundle) {
    var mercadolibre = config.mercadolibre
    var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == bundle.mercadolibreConnection.country.toLowerCase()})
    var options = {
        method: 'POST',
        uri: _config.apiBaseUrl + '/items',
        qs: {
                access_token: bundle.mercadolibreConnection.access_token
            },
        body: bundle._product,
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
      .then(function(result){
        bundle.productUploadResponse = result
        return bundle
      })
  };
}
function saveProductUploadContent() {
  return function(bundle) {
    var updates = {
      externalId: bundle.productUploadResponse.id,
      externalContent: JSON.stringify(bundle.productUploadResponse)
    }
    return bundle.productLink.updateAttributes(updates).then(function(result){
      return bundle
    })
  }
}
function storeSuccessLogEntry() {
  return function(bundle) {
    var logEntry = {}
    logEntry.type = ProductLinkLogEntry.TYPES.INFO,
    logEntry.code = ProductLinkLogEntry.CODES.PRODUCT_CREATED_REMOTE
    logEntry.content = JSON.stringify(bundle.productUploadResponse)
    logEntry.MarketplaceConnectionId = bundle.mercadolibreConnection._id
    logEntry.ProductLinkId = bundle.productLink._id
    logEntry.MerchantId = bundle.mercadolibreConnection.MerchantId

    return ProductLinkLogEntry.create(logEntry).then(function(result){
      return bundle
    })
  }
}


function handleExpiredTokenError(data, fn) {//Hacker AF
  return function(err) {
    if(err.error && err.error.message == 'invalid_token') {
      return MarketplaceConnection.find({
        where: {
          _id: req.params.id
        }
      }).then(function(marketplaceConnection){
        var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
        var options = {
            method: 'POST',
            body: {},
            uri: _config.apiBaseUrl + MercadolibreApiPaths.AUTH_TOKEN,
            qs: {
              grant_type: 'refresh_token',
              client_id: _config.appId,
              client_secret: _config.secretKey,
              refresh_token: marketplaceConnection.refresh_token
            },
            json: true // Automatically parses the JSON string in the response
        };
        return rp(options).then(function(result){//Once the refresh token is aquired, update the connection tokens and repeat the entire request.
          marketplaceConnection.updateAttributes(result).then(function(result){
            fn(data)
          })

        })
      })
    }
    throw err
  };
}

