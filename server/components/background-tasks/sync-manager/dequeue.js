'user strict'


import _ from 'lodash';
import nr from 'node-resque';
import config from '../../../config/environment';
import redis from 'redis';
import mercadolibreSyncManager from './mercadolibre/queue'
import dafitiSyncManager from './dafiti/queue'
import {Product} from '../../../sqldb';
import {Marketplace} from '../../../sqldb';
import {MarketplaceConnection} from '../../../sqldb';
import {Merchant} from '../../../sqldb';
var Promise = require('promise');

var deliverNewProductForSynchronization = function(product, callback){
    return Product.find({
      where: {
        _id: product._id
      }
    })
    .then(initializeBundle())
    .then(findProductMerchant())
    .then(findMarketplaceConnections())
    .then(queueNewProductSynchronizationTasks())
    .then(function(result){ callback(null) })
    .catch(function(err){ callback(err) })
}
function initializeBundle(){
  return function(product){
    var bundle = {} //The bundle object will collect result while going through promises
    bundle.product = product;
    return bundle
  }
}
function findProductMerchant() {
  return function(bundle) {
    return Merchant.find({
      where: {
        _id: bundle.product.MerchantId
      }
    })
    .then(function(result){
      bundle.merchant = result;
      return bundle
    })
  };
}
function findMarketplaceConnections() {
  return function(bundle) {
    return MarketplaceConnection.findAll({
      where: {
        MerchantId: bundle.merchant._id,
        status: 'created'
      }
    })
    .then(function(result){
      bundle.marketplaceConnections = result;
      return bundle
    })
  };
}
function findMarketplaceConnection(marketplaceConnectionId) {
  return function(bundle) {
    return MarketplaceConnection.find({
      where: {
        _id: marketplaceConnectionId,
        status: 'created'
      }
    })
    .then(function(result){
      bundle.marketplaceConnections = [result];
      return bundle
    })
  };
}
function queueNewProductSynchronizationTasks() {
  return function(bundle) {
    _.each(bundle.marketplaceConnections, function(item){
      if(item.provider === Marketplace.Items.MERCADOLIBRE.toLowerCase()) { queueNewProductForMercadolibreSynchronization(bundle.product, item)}
      if(item.provider === Marketplace.Items.DAFITI.toLowerCase()) { queueNewProductForDafitiSynchronization(bundle.product, item)}
    })
    return bundle;
  };
}
function queueNewProductForMercadolibreSynchronization(product, marketplaceConnection) {
  var task = {
    ProductId: product._id,
    MarketplaceConnectionId: marketplaceConnection._id,
    task: 'create_new_product'
  }
  mercadolibreSyncManager.queueNewProductForSynchronization(task)
}
function queueNewProductForDafitiSynchronization(product, marketplaceConnection) {
  var task = {
    ProductId: product._id,
    MarketplaceConnectionId: marketplaceConnection._id,
    task: 'create_new_product'
  }
  dafitiSyncManager.queueNewProductForSynchronization(task)
}

var deliverNewProductForSynchronizationInMarketplace = function(product, marketplaceConnection, user, callback){
    return Product.find({
      where: {
        _id: product._id
      }
    })
    .then(initializeBundle())
    .then(findProductMerchant())
    .then(findMarketplaceConnections())
    .then(queueNewProductSynchronizationTasks())
    .then(function(result){ callback(null) })
    .catch(function(err){ callback(err) })
}

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }

  var jobs = {
    "deliverNewProductForSynchronization": { perform: deliverNewProductForSynchronization },
    "deliverNewProductForSynchronizationInMarketplace": { perform: deliverNewProductForSynchronizationInMarketplace },
  }
  var worker = new nr.worker({connection: { redis: redisClient }, queues: ['sync_tasks_master']}, jobs);
  worker.connect(function(){
    worker.workerCleanup(); // optional: cleanup any previous improperly shutdown workers on this host
    worker.start();
  });


  worker.on('start',           function(){ console.log("Synchronization manager started"); });
  worker.on('end',             function(){ console.log("Synchronization manager ended"); });
  worker.on('cleaning_worker', function(worker, pid){ console.log("cleaning old synchronization manager " + worker); });
  worker.on('poll',            function(queue){  /*console.log("worker polling " + queue);*/});
  worker.on('job',             function(queue, job){ console.log("Synchronization manager job " + queue + " " + JSON.stringify(job)); });
  worker.on('reEnqueue',       function(queue, job, plugin){ console.log("reEnqueue job (" + plugin + ") " + queue + " " + JSON.stringify(job)); });
  worker.on('success',         function(queue, job, result){ console.log("job success " + queue + " " + JSON.stringify(job) + " >> " + result); });
  worker.on('failure',         function(queue, job, failure){ console.log("job failure " + queue + " " + JSON.stringify(job) + " >> " + failure); });
  worker.on('error',           function(queue, job, error){ console.log("error " + queue + " " + JSON.stringify(job) + " >> " + error); });
  worker.on('pause',           function(){  /*console.log("worker paused");*/ });


