'user strict'


import nr from 'node-resque';
import config from '../../../config/environment';
import redis from 'redis';

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }

  var queue = new nr.queue({connection: connectionDetails});
  queue.on('error', function(error){ console.log(error); });

  exports.queueNewProductForSynchronizationInMarketplace = function(product, marketplaceConnection, user){
    var params = [product, marketplaceConnection, user]
    queue.connect(function(){
      queue.enqueue('sync_tasks_master', "deliverNewProductForSynchronizationInMarketplace", params);
    });
  }

