'user strict'

import _ from 'lodash';
import nr from 'node-resque';
import config from '../../../../config/environment';
import mercadolibreConnector from '../../../connect/mercadolibre';
import redis from 'redis';
import * as mercadolibreSyncManager from './queue'
import {MercadolibreConnectionConfiguration} from '../../../../sqldb';
import {MarketplaceConnection} from '../../../../sqldb';
import {ProductLink} from '../../../../sqldb';
import {Product} from '../../../../sqldb';
import {User} from '../../../../sqldb';

var Promise = require('promise');

var handleNewProductForSynchronization = function(product, marketplaceConnection, user, callback){
    console.log('Handling new product ' + JSON.stringify(product, marketplaceConnection, user) + ' for sync in Mercadolibre')
    mercadolibreSyncManager.queueUploadNewProductTask(product, marketplaceConnection, user)
    callback(null)
}
var handleStartSynchronizationProcessTask = function(mercadolibreConnection, user, callback){
    console.log('Handling start synchronization process with parameters' + JSON.stringify(mercadolibreConnection))
    MarketplaceConnection.find({
      where: {
        _id: mercadolibreConnection.MarketplaceConnectionId
      }
    })
    .then(initializeStartSynchronizationProcessTaskBundle())
    .then(findUser(user))
    .then(findMercadolibreConnectionConfiguration())
    .then(findAlreadyExistingMercadolibreProducts())
    .then(defineSynchronizationTasksForStartSynchronizationProcessTask())
    .then(queueUploadNewProductsToMercadolibre())
    .then(function(result){ callback(null) })
    .catch(function(err){ console.log(err); callback(err) })
}
function initializeStartSynchronizationProcessTaskBundle(){
  return function(mercadolibreConnection){
    var bundle = {} //The bundle object will collect result while going through promises
    bundle.mercadolibreConnection = mercadolibreConnection;
    return bundle
  }
}
function findUser(user) {
  return function(bundle) {
    return User.find({
      where: {
        _id: user.UserId
      },
      attributes: ['_id', 'name', 'email']
    })
    .then(function(result){
      bundle.user = result;
      return bundle
    })
  };
}
function findMercadolibreConnectionConfiguration() {
  return function(bundle) {
    return MercadolibreConnectionConfiguration.find({
      where: {
        MarketplaceConnectionId: bundle.mercadolibreConnection._id
      }
    })
    .then(function(result){
      bundle.mercadolibreConnectionConfiguration = result;
      return bundle
    })
  };
}
function findAlreadyExistingMercadolibreProducts() {
  return function(bundle) {
    return ProductLink.findAll({
      where: {
        MarketplaceConnectionId: bundle.mercadolibreConnection._id,
      }
    })
    .then(function(result){
      bundle.mercadolibreProducts = result;
      return bundle
    })
  };
}
function defineSynchronizationTasksForStartSynchronizationProcessTask() {
  return function(bundle) {
    //All products without mercadolibre mapping must be uploaded.
    bundle.tasksDefinition = {
      upload_new_products: true
    }
    return bundle;
  };
}
function queueUploadNewProductsToMercadolibre() {
  return function(bundle) {
    bundle.tasks = bundle.tasks || []
    if(bundle.tasksDefinition.upload_new_products){
      return ProductLink.findAll({
        where: {
          MarketplaceConnectionId: bundle.mercadolibreConnection._id,
          MerchantId: bundle.mercadolibreConnection.MerchantId,
          externalId: {
            $eq: null
          }
        }
      })
      .then(function(result){
        var _mercadolibre_products = _.map(result, function(item){ return item.get() });
        _.each(_mercadolibre_products, function(item){
          mercadolibreSyncManager.queueUploadNewProductTask(item, bundle.mercadolibreConnection, bundle.user)
        })
        return bundle
      })
    } else {
      return bundle;
    }

  };
}

var handleUploadNewProductTask = function(mercadolibreConnection, productLink, user, callback){
    console.log('Handling upload new product ' + JSON.stringify(productLink) + ' for sync in Mercadolibre')
    mercadolibreConnector.uploadProduct(mercadolibreConnection, productLink, user)
    callback(null)
}

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }
  var jobs = {
    "handleNewProductForSynchronization": { perform: handleNewProductForSynchronization },
    "handleStartSynchronizationProcessTask": { perform: handleStartSynchronizationProcessTask },
    "handleUploadNewProductTask": { perform: handleUploadNewProductTask },
  }
  var worker = new nr.worker({connection: { redis: redisClient }, queues: ['sync_tasks_mercadolibre']}, jobs);
  worker.connect(function(){
    worker.workerCleanup(); // optional: cleanup any previous improperly shutdown workers on this host
    worker.start();
  });


  worker.on('start',           function(){ console.log("Synchronization manager for Mercadolibre started"); });
  worker.on('end',             function(){ console.log("Synchronization manager for Mercadolibre ended"); });
  worker.on('cleaning_worker', function(worker, pid){ console.log("cleaning old synchronization manager " + worker); });
  worker.on('poll',            function(queue){  /*console.log("worker polling " + queue);*/});
  worker.on('job',             function(queue, job){ console.log("Synchronization manager for Mercadolibre job " + queue + " " + JSON.stringify(job)); });
  worker.on('reEnqueue',       function(queue, job, plugin){ console.log("reEnqueue job (" + plugin + ") " + queue + " " + JSON.stringify(job)); });
  worker.on('success',         function(queue, job, result){ console.log("job success " + queue + " " + JSON.stringify(job) + " >> " + result); });
  worker.on('failure',         function(queue, job, failure){ console.log("job failure " + queue + " " + JSON.stringify(job) + " >> " + failure); });
  worker.on('error',           function(queue, job, error){ console.log("error " + queue + " " + JSON.stringify(job) + " >> " + error); });
  worker.on('pause',           function(){  /*console.log("worker paused");*/ });


