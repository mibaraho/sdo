'user strict'


import nr from 'node-resque';
import config from '../../../../config/environment';
import redis from 'redis';

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }

  var queue = new nr.queue({connection: connectionDetails});
  queue.on('error', function(error){ console.log(error); });

  exports.queueNewProductForSynchronization = function(product, marketplaceConnection, user){
    var user = { UserId: user._id }
    var product = {ProductId: productId}
    var marketplaceConnection = { MarketplaceConnectionId: marketplaceConnectionId }
    var params = [mercadolibreConnection, mercadolibreProduct, user]
    queue.connect(function(){
      queue.enqueue('sync_tasks_mercadolibre', "handleNewProductForSynchronization", params);
    });
  }

  exports.queueStartSynchronizationProcessTask = function(mercadolibreConnection, user){
    var mercadolibreConnection = { MarketplaceConnectionId: mercadolibreConnection._id}
    var user = { UserId: user._id }
    var params = [mercadolibreConnection, user]
    queue.connect(function(){
      queue.enqueue('sync_tasks_mercadolibre', "handleStartSynchronizationProcessTask", params);
    });
  }

  exports.queueUploadNewProductTask = function(productLink, mercadolibreConnection, user){
    var mercadolibreConnection = { MarketplaceConnectionId: mercadolibreConnection._id}
    var user = { UserId: user._id }
    var productLink = { ProductLinkId: productLink._id }
    var params = [mercadolibreConnection, productLink, user]
    queue.connect(function(){
      queue.enqueue('sync_tasks_mercadolibre', "handleUploadNewProductTask", params);
    });
  }

