'user strict'


import nr from 'node-resque';
import config from '../../../config/environment';
import * as mailer from '../../mailers/mailer';
import {EmailTemplate} from '../../../sqldb';
import {Merchant} from '../../../sqldb';
import {MerchantEmailConfiguration} from '../../../sqldb';
import {MerchantEmailTemplate} from '../../../sqldb';
import {PlatformConfiguration} from '../../../sqldb';
import {ResetPasswordRequest} from '../../../sqldb';
import {User} from '../../../sqldb';
import {UserInvitation} from '../../../sqldb';
import redis from 'redis';
var Promise = require('promise');

  var sendEmailToUserWhenCreatesAnAccount = function(user, applicationUrl, callback){
   return User.find({
        where: {
          _id: user._id
        }
      })
      .then(initializeUserAccountCreatedBundle())
      .then(findEmailTemplate(EmailTemplate.EMAIL_CODES.EMAIL_TO_USER_WHEN_CREATES_AN_ACCOUNT))
      .then(function(bundle){
          var options = {}
          if(!bundle.isValid) {
            callback(null)
            return;
          }
          if(!bundle.emailTemplate) {
            callback(null)
            return;
          }
          if(bundle.emailConfiguration){
            options.from = bundle.emailConfiguration.defaultSenderName + '<' + bundle.emailConfiguration.defaultSenderEmail + '>'
          }
          options.subject = bundle.emailTemplate.subject.split('_USER_NAME').join(bundle.user.name)
          options.to = bundle.user.email
          options.html = bundle.emailTemplate.htmlBody.split('_USER_NAME').join(bundle.user.name)
                                                .split('_APPLICATION_URL').join(applicationUrl)
          mailer.send(options, callback)

      })
      .catch(function(err){
        console.log(err)
      })
    }
  function initializeUserAccountCreatedBundle(){
    return function(user){
      var bundle = {}
      bundle.user = user
      if(bundle.user){
        bundle.isValid = true
      }
      return bundle
    }
  }
  var sendEmailToUserWhenRequestResetPassword = function(resetPasswordRequest, applicationUrl, resetPasswordUrl, callback){
   return ResetPasswordRequest.find({
        where: {
          _id: resetPasswordRequest.ResetPasswordRequestId
        },
        include: [{
          model: User,
          attributes: ['_id', 'name', 'email']
        }]
      })
      .then(initializeResetPasswordRequestBundle())
      .then(findEmailTemplate(EmailTemplate.EMAIL_CODES.EMAIL_TO_USER_WHEN_REQUEST_RESET_PASSWORD))
      .then(function(bundle){
          var options = {}
          if(!bundle.isValid) {
            callback(null)
            return;
          }
          if(!bundle.emailTemplate) {
            callback(null)
            return;
          }
          if(bundle.emailConfiguration){
            options.from = bundle.emailConfiguration.defaultSenderName + '<' + bundle.emailConfiguration.defaultSenderEmail + '>'
          }
          options.subject = bundle.emailTemplate.subject.split('_USER_NAME').join(bundle.user.name)
          options.to = bundle.user.email
          options.html = bundle.emailTemplate.htmlBody.split('_USER_NAME').join(bundle.user.name)
                                                .split('_RESET_PASSWORD_URL').join(resetPasswordUrl)
          mailer.send(options, callback)

      })
      .catch(function(err){
        console.log(err)
      })
    }

  function initializeResetPasswordRequestBundle(){
    return function(resetPasswordRequest){
      var bundle = {}
      bundle.user = resetPasswordRequest.User
      bundle.resetPasswordRequest = resetPasswordRequest
      if(bundle.user){
        bundle.isValid = true
      }
      return bundle
    }
  }
  var sendEmailWhenLeadIsCreatedFromOutside = function(lead, callback){
      var options = {}
      var promises = []

      var emailTemplatePromise = EmailTemplate.find({
        where: {
          code: mailer.EMAIL_CODES.EMAIL_TO_STAFF_WHEN_LEAD_IS_GENERATED_FROM_OUTSIDE
        }
      })
      promises.push(emailTemplatePromise)
      var destination = PlatformConfiguration.find({
        where: {
          code: 'MAIN_SALES_EMAIL'
        }
      })
      promises.push(destination)
      Promise.all(promises).then(function(result){
        var emailTemplate = result[0];
        var destination = result[1];
        options.subject = emailTemplate.subject
        options.to = destination.value_text
        options.html = emailTemplate.htmlBody.split('_LEAD_NAME').join(lead.name)
                                             .split('_LEAD_PHONE_NUMBER').join(lead.phoneNumber)
                                             .split('_LEAD_EMAIL').join(lead.email)
                                             .split('_LEAD_MESSAGE').join(lead.message)
                                             .split('_LEAD_SOURCE').join(lead.source);

        mailer.send(options, callback)
      })
      .catch(function(err){
        console.log(err)
      })
    }
  var sendEmailWhenContactIsRequestedFromOutside = function(contact, callback){
      var options = {}

      var promises = []
      var emailTemplatePromise = EmailTemplate.find({
        where: {
          code: mailer.EMAIL_CODES.EMAIL_TO_STAFF_WHEN_CONTACT_IS_REQUESTED
        }
      })
      promises.push(emailTemplatePromise)
      var destination = PlatformConfiguration.find({
        where: {
          code: 'MAIN_CONTACT_EMAIL'
        }
      })
      promises.push(destination)
      Promise.all(promises).then(function(result){
        var emailTemplate = result[0];
        var destination = result[1];
        options.subject = emailTemplate.subject
        options.to = destination.value_text
        options.html = emailTemplate.htmlBody.split('_CONTACT_NAME').join(contact.name)
                                             .split('_CONTACT_PHONE_NUMBER').join(contact.phoneNumber)
                                             .split('_CONTACT_EMAIL').join(contact.email)
                                             .split('_CONTACT_MESSAGE').join(contact.message)
                                             .split('_CONTACT_SOURCE').join(contact.source);
        mailer.send(options, callback)
      })
      .catch(function(err){
        console.log(err)
      })
    }

  var sendEmailToInvitedWhenInvitationIsCreated = function(invitation, applicationUrl, invitationsUrl, signupByInvitationUrl, callback){
   return UserInvitation.find({
        where: {
          _id: invitation._id
        }
      })
      .then(initializeUserInvitationBundle())
      .then(findTargetUser())
      .then(defineEmailToUser())
      .then(findMerchant())
      .then(findMerchantEmailConfiguration())
      .then(findMerchantEmailTemplate(EmailTemplate.EMAIL_CODES.EMAIL_TO_INVITED_WHEN_INVITATION_IS_CREATED))
      .then(findEmailTemplate(EmailTemplate.EMAIL_CODES.EMAIL_TO_INVITED_WHEN_INVITATION_IS_CREATED))
      .then(function(bundle){
          var options = {}
          if(!bundle.isValid) {
            callback(null)
            return;
          }
          if(!bundle.emailTemplate) {
            callback(null)
            return;
          }

          bundle.invitation.message = bundle.invitation.message || ''
          if(bundle.emailConfiguration){
            options.from = bundle.emailConfiguration.defaultSenderName + '<' + bundle.emailConfiguration.defaultSenderEmail + '>'
          }
          options.subject = bundle.emailTemplate.subject.split('_MERCHANT_NAME').join(bundle.merchant.name)
          options.to = bundle.invitation.email
          options.html = bundle.emailTemplate.htmlBody.split('_TARGET_NAME').join(bundle.invitation.name)
                                                .split('_MERCHANT_NAME').join(bundle.merchant.name)
                                                .split('_MESSAGE').join(bundle.invitation.message)
                                                .split('_APPLICATION_URL').join(applicationUrl)
                                                .split('_INVITATIONS_URL').join(invitationsUrl)
                                                .split('_SIGNUP_BY_INVITATION_URL').join(signupByInvitationUrl)
          mailer.send(options, callback)

      })
      .catch(function(err){
        console.log(err)
      })
    }

  function initializeUserInvitationBundle(){
    return function(invitation){
      var bundle = {}
      bundle.invitation = invitation
      if(bundle.invitation){
        bundle.isValid = true
        bundle.MerchantId = invitation.MerchantId
      }
      return bundle
    }
  }

  function findMerchant(){
    return function(bundle){
      return Merchant.find({
        where: {
          _id: bundle.MerchantId
        }
      }).then(function(result){
        bundle.merchant = result
        return bundle
      })
    }
  }

  function findTargetUser(){
    return function(bundle){
      return User.find({
        where: {
          email: bundle.invitation.email
        },
        attributes: ['_id', 'name', 'email']
      }).then(function(result){
        bundle.targetUser = result
        return bundle
      })
    }
  }

  function defineEmailToUser(){
    return function(bundle){
      if(bundle.targetUser){
        bundle.emailCode = EmailTemplate.EMAIL_CODES.EMAIL_TO_EXISTING_USER_WHEN_INVITATION_IS_CREATED
      } else {
        bundle.emailCode = EmailTemplate.EMAIL_CODES.EMAIL_TO_INVITED_WHEN_INVITATION_IS_CREATED
      }
      return bundle
    }
  }

  function findMerchantEmailConfiguration(){
    return function(bundle){
      if(!bundle.isValid) {return;}
      return MerchantEmailConfiguration.find({
        where: {
          MerchantId: bundle.MerchantId,
          status: 'created'
        }
      }).then(function(result){
        bundle.emailConfiguration = result
        return bundle
      })
    }
  }
  function findMerchantEmailTemplate(_code){
    return function(bundle){
      if(!bundle.isValid) {return;}
      if(bundle.emailCode) { _code = bundle.emailCode;}
      return MerchantEmailTemplate.find({
        where: {
          MerchantId: bundle.MerchantId,
          status: 'created',
          code: _code
        }
      }).then(function(result){
        bundle.emailTemplate = result
        return bundle
      })
    }
  }
  function findEmailTemplate(_code){
    return function(bundle){
      if(bundle.emailTemplate) {return bundle;}
      if(bundle.emailCode) { _code = bundle.emailCode;}
      return EmailTemplate.find({
        where: {
          status: 'created',
          code: _code
        }
      }).then(function(result){
        bundle.emailTemplate = result
        return bundle
      })
    }
  }

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }
  var jobs = {
    "sendEmailWhenLeadIsCreatedFromOutside": { perform: sendEmailWhenLeadIsCreatedFromOutside },
    "sendEmailWhenContactIsRequestedFromOutside": { perform: sendEmailWhenContactIsRequestedFromOutside },
    "sendEmailToInvitedWhenInvitationIsCreated": { perform: sendEmailToInvitedWhenInvitationIsCreated },
    "sendEmailToUserWhenCreatesAnAccount": { perform: sendEmailToUserWhenCreatesAnAccount },
    "sendEmailToUserWhenRequestResetPassword": { perform: sendEmailToUserWhenRequestResetPassword }
  }
  var worker = new nr.worker({connection: { redis: redisClient }, queues: ['emails']}, jobs);
  worker.connect(function(){
    worker.workerCleanup(); // optional: cleanup any previous improperly shutdown workers on this host
    worker.start();
  });


  worker.on('start',           function(){ console.log("Mailer started"); });
  worker.on('end',             function(){ console.log("Mailer ended"); });
  worker.on('cleaning_worker', function(worker, pid){ console.log("cleaning old mailer " + worker); });
  worker.on('poll',            function(queue){  /*console.log("worker polling " + queue); */});
  worker.on('job',             function(queue, job){ console.log("working job " + queue + " " + JSON.stringify(job)); });
  worker.on('reEnqueue',       function(queue, job, plugin){ console.log("reEnqueue job (" + plugin + ") " + queue + " " + JSON.stringify(job)); });
  worker.on('success',         function(queue, job, result){ console.log("job success " + queue + " " + JSON.stringify(job) + " >> " + result); });
  worker.on('failure',         function(queue, job, failure){ console.log("job failure " + queue + " " + JSON.stringify(job) + " >> " + failure); });
  worker.on('error',           function(queue, job, error){ console.log("error " + queue + " " + JSON.stringify(job) + " >> " + error); });
  worker.on('pause',           function(){  /*console.log("worker paused");*/ });


