'user strict'

import _ from 'lodash'
import nr from 'node-resque';
import config from '../../../config/environment';
import redis from 'redis';

  var redisClient =  redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.password, tls: config.redis.tls});
  var connectionDetails = { redis: redisClient }

  var queue = new nr.queue({connection: connectionDetails});
  queue.on('error', function(error){ console.log(error); });

  exports.enqueueEmailWhenLeadIsCreatedFromOutside = function(lead){
    var params = [lead]
    queue.connect(function(){
      if(config.sendEmails){
        queue.enqueue('emails', "sendEmailWhenLeadIsCreatedFromOutside", params);
      }
    });
  }
  exports.enqueueEmailWhenContactIsRequestedFromOutside = function(contact){
    var params = [contact]
    queue.connect(function(){
      if(config.sendEmails){
        queue.enqueue('emails', "sendEmailWhenContactIsRequestedFromOutside", params);
      }
    });
  }


  exports.queueEmailToInvitedWhenInvitationIsCreated = function(invitation){
    var applicationUrl = buildApplicationUrl()
    var invitationsUrl = buildInvitationsUrl()
    var signupByInvitationUrl = buildSignupByInvitationUrl()
    invitation = _.pick(invitation, '_id')
    var params = [invitation, applicationUrl, invitationsUrl, signupByInvitationUrl]
    queue.connect(function(){
      if(config.sendEmails){
        queue.enqueue('emails', "sendEmailToInvitedWhenInvitationIsCreated", params);
      }
    });
  }

  exports.queueEmailToUserWhenCreatesAnAccount = function(user){
    var applicationUrl = buildApplicationUrl()
    user = _.pick(user, '_id')
    var params = [user, applicationUrl]
    queue.connect(function(){
      if(config.sendEmails){
        queue.enqueue('emails', "sendEmailToUserWhenCreatesAnAccount", params);
      }
    });
  }

  exports.queueEmailToUserWhenRequestResetPassword = function(requestPasswordReset){
    var applicationUrl = buildApplicationUrl()
    var resetPasswordUrl = buildResetPasswordUrl(requestPasswordReset)
    requestPasswordReset.ResetPasswordRequestId = requestPasswordReset._id
    requestPasswordReset = _.pick(requestPasswordReset, 'ResetPasswordRequestId')
    var params = [requestPasswordReset, applicationUrl, resetPasswordUrl]
    queue.connect(function(){
      if(config.sendEmails){
        queue.enqueue('emails', "sendEmailToUserWhenRequestResetPassword", params);
      }
    });
  }

  //Utils
  var buildApplicationUrl = function() {
    var url = '';
    url += config.host.protocol
    url += '://'
    url += config.host.domain
    url += config.host.port ? ':' + config.host.port : ''
    return url;
  }
  var buildResetPasswordUrl = function(requestPasswordReset) {
    var url = buildApplicationUrl()
    url += '/reset-password/'+requestPasswordReset._id+'/perform'
    return url;
  }
  var buildSignupByInvitationUrl = function() {
    var url = buildApplicationUrl()
    url += '/signup-by-invitation'
    return url;
  }
  var buildInvitationsUrl = function() {
    var url = buildApplicationUrl()
    url += '/users/me/invitations'
    return url;
  }
