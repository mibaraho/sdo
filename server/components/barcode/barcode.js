'use strict'

  var bwipjs = require('bwip-js');

  var DEFAULT_AND_MAX_HEIGHT = 10;
  var DEFAULT_AND_MAX_SCALE = 3;


export function generate(data, options, writeStream) {

  var height = options.height || DEFAULT_AND_MAX_HEIGHT;
  var height = height > DEFAULT_AND_MAX_HEIGHT ? DEFAULT_AND_MAX_HEIGHT : height

  var scale = options.scale || DEFAULT_AND_MAX_SCALE;
  var scale = scale > DEFAULT_AND_MAX_SCALE ? DEFAULT_AND_MAX_SCALE : scale
  bwipjs.toBuffer({
      bcid:        'ean13',    // Barcode type
      text:        data.code, // Text to encode
      scale:       scale,        // x scaling factor
      height:      height,       // Bar height, in millimeters
      includetext: true,     // Show human-readable text
      textxalign:  'center',   // Always good to set this
      textsize:    13        // Font size, in points
    }, function (err, png) {
      if (err) {
        throw err
      } else {
        writeStream.end(png);
      }
    });
}
