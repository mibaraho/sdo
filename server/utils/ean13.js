'use strict'

import _ from 'lodash';

export function generate(){
  var min = 100000000000;
  var max = 999999999999;
  var num = Math.floor(Math.random() * (max - min + 1)) + min;
  var str = num.toString();
  var arr = str.split('');
  var rev = arr.reverse();
  var evens = _.at(rev, 1, 3, 5, 7, 9, 11)
  var odds = _.at(rev, 0, 2, 4, 6, 8, 10)
  evens = _.map(evens, function(n){
    return parseInt(n);
  })
  odds = _.map(odds, function(n){
    return parseInt(n);
  })
  var sumEvens = _.sum(evens)
  var sumOdds = _.sum(odds)
  var totalOdds = sumOdds*3;
  var total = totalOdds+sumEvens;
  var ceil = Math.ceil(total/10) * 10
  var checksum = ceil - total
  var final = str + checksum.toString()

  return final
}
