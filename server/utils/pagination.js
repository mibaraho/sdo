'use strict'

var DEFAULT_OFFSET = 0;
var DEFAULT_PAGE_SIZE =50;

export function paginate(page_number, total_entries){

  page_number = page_number || 1; //Default value
  page_number = page_number > 0 ? page_number : 1 //Negative numbers will be shifted to first page
  page_number = parseInt(page_number) //Cast to integer
  var result = {}
  //Calculate offset
  result.offset = (page_number - 1) * DEFAULT_PAGE_SIZE;
  result.limit = DEFAULT_PAGE_SIZE;
  result.total_pages = Math.ceil(total_entries / DEFAULT_PAGE_SIZE);
  result.current_page = page_number
  result.next_page = page_number == result.total_pages ? 0 : page_number + 1
  result.previous_page = page_number == 1 ? 0 : page_number - 1
  result.total_items = total_entries

  return result
}
