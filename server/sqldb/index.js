/**
 * Sequelize initialization module
 */

'use strict';

import path from 'path';
import config from '../config/environment';
import Sequelize from 'sequelize';

var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};

// Insert models below
db.AutoResponse = db.sequelize.import('../api/auto-response/auto-response.model');
db.MercadolibreConnectionConfiguration = db.sequelize.import('../api/mercadolibre-connection-configuration/mercadolibre-connection-configuration.model');
db.MerchantUser = db.sequelize.import('../api/merchant-user/merchant-user.model');
db.Marketplace = db.sequelize.import('../api/marketplace/marketplace.model');
db.MarketplaceConnection = db.sequelize.import('../api/marketplace-connection/marketplace-connection.model');
db.Merchant = db.sequelize.import('../api/merchant/merchant.model');
db.Thing = db.sequelize.import('../api/thing/thing.model');
db.User = db.sequelize.import('../api/user/user.model');

export default db;
