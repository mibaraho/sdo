/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/auto-responses              ->  index
 * POST    /api/auto-responses              ->  create
 * GET     /api/auto-responses/:id          ->  show
 * PUT     /api/auto-responses/:id          ->  update
 * DELETE  /api/auto-responses/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {AutoResponse} from '../../sqldb';
import config from '../../config/environment';

var ConversationV1 = require('watson-developer-cloud/conversation/v1');
var LanguageTranslatorV2 = require('watson-developer-cloud/language-translator/v2');
var ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');

var MAX_ANGER_THRESHOLD = 0.5;
var MAX_SADNESS_THRESHOLD = 0.5;
var MIN_JOY_THRESHOLD = 0.5;


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err)
    res.status(statusCode).send(err);
  };
}

// Gets a list of AutoResponses
export function index(req, res) {
  AutoResponse.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a list of AutoResponses
export function ask(req, res) {
   var conversation = new ConversationV1(config.watson.conversation);
   conversation.message({
     input: {
       text: "hi"
     },
     workspace_id:config.watson.conversation.workspace_id,
     context: {
       system: {
         dialog_stack: [
           "root"
         ],
         dialog_turn_counter: 1,
         dialog_request_counter: 1
       },
       default_counter: 0
     }
   }, function(err, response) {
     if (err) {
       throw err
     } else {
       conversation.message({
           input: {
             text: req.body.question
           },
           workspace_id:config.watson.conversation.workspace_id,
           context: {
             system: {
               dialog_stack: [
                 "root"
               ],
               dialog_turn_counter: 1,
               dialog_request_counter: 1
             },
             default_counter: 0
           }
         }, function(err, response2) {
             if (err) {
               throw err
             } else {
               return res.send(response2)
             }
           })
     }
   })


}

export function translateAndAnalyze(req, res) {
  var language_translator = new LanguageTranslatorV2({
    username: '27ae5137-a20c-4f3c-8bd5-217991328e57',
    password: 'WxNybO82fBs4',
    url: 'https://gateway.watsonplatform.net/language-translator/api'
  });

  var tone_analyzer = new ToneAnalyzerV3({
    username: '0b103c74-f2d6-4e33-8b85-66edb35fd11d',
    password: 'jd0jC4kPhRvD',
    version_date: '2016-05-19'
  });

  language_translator.translate({
    text: req.body.text, source : 'es', target: 'en' },
    function (err, translation) {
      if (err){
        console.log('error:', err);
      }
      else{
      tone_analyzer.tone({ text: _.first(translation.translations).translation },
        function(err, tone) {
          if (err){
            console.log(err);
          }
          else{
            res.send(tone)
          }
      });
      }
  });


}



// Gets a single AutoResponse from the DB
export function show(req, res) {
  AutoResponse.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new AutoResponse in the DB
export function create(req, res) {
  AutoResponse.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing AutoResponse in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  AutoResponse.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a AutoResponse from the DB
export function destroy(req, res) {
  AutoResponse.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
