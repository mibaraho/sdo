'use strict';

var app = require('../..');
import request from 'supertest';

var newAutoResponse;

describe('AutoResponse API:', function() {

  describe('GET /api/auto-responses', function() {
    var autoResponses;

    beforeEach(function(done) {
      request(app)
        .get('/api/auto-responses')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          autoResponses = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      autoResponses.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/auto-responses', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/auto-responses')
        .send({
          name: 'New AutoResponse',
          info: 'This is the brand new autoResponse!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newAutoResponse = res.body;
          done();
        });
    });

    it('should respond with the newly created autoResponse', function() {
      newAutoResponse.name.should.equal('New AutoResponse');
      newAutoResponse.info.should.equal('This is the brand new autoResponse!!!');
    });

  });

  describe('GET /api/auto-responses/:id', function() {
    var autoResponse;

    beforeEach(function(done) {
      request(app)
        .get('/api/auto-responses/' + newAutoResponse._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          autoResponse = res.body;
          done();
        });
    });

    afterEach(function() {
      autoResponse = {};
    });

    it('should respond with the requested autoResponse', function() {
      autoResponse.name.should.equal('New AutoResponse');
      autoResponse.info.should.equal('This is the brand new autoResponse!!!');
    });

  });

  describe('PUT /api/auto-responses/:id', function() {
    var updatedAutoResponse;

    beforeEach(function(done) {
      request(app)
        .put('/api/auto-responses/' + newAutoResponse._id)
        .send({
          name: 'Updated AutoResponse',
          info: 'This is the updated autoResponse!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedAutoResponse = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAutoResponse = {};
    });

    it('should respond with the updated autoResponse', function() {
      updatedAutoResponse.name.should.equal('Updated AutoResponse');
      updatedAutoResponse.info.should.equal('This is the updated autoResponse!!!');
    });

  });

  describe('DELETE /api/auto-responses/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/auto-responses/' + newAutoResponse._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when autoResponse does not exist', function(done) {
      request(app)
        .delete('/api/auto-responses/' + newAutoResponse._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
