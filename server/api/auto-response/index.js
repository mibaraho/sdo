'use strict';

var express = require('express');
var controller = require('./auto-response.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

//router.get('/auto-responses/', controller.index);
router.post('/auto-responses/ask', controller.ask);
router.post('/auto-responses/translate-and-analyze', controller.translateAndAnalyze);
//router.get('/auto-responses/:id', controller.show);
//router.post('/auto-responses/', controller.create);
//router.put('/auto-responses/:id', controller.update);
//router.patch('/auto-responses/:id', controller.update);
//router.delete('/auto-responses/:id', controller.destroy);

module.exports = router;
