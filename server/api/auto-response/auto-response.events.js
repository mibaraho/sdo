/**
 * AutoResponse model events
 */

'use strict';

import {EventEmitter} from 'events';
var AutoResponse = require('../../sqldb').AutoResponse;
var AutoResponseEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AutoResponseEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  AutoResponse.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    AutoResponseEvents.emit(event + ':' + doc._id, doc);
    AutoResponseEvents.emit(event, doc);
    done(null);
  }
}

export default AutoResponseEvents;
