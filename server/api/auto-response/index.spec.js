'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var autoResponseCtrlStub = {
  index: 'autoResponseCtrl.index',
  show: 'autoResponseCtrl.show',
  create: 'autoResponseCtrl.create',
  update: 'autoResponseCtrl.update',
  destroy: 'autoResponseCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var autoResponseIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './auto-response.controller': autoResponseCtrlStub
});

describe('AutoResponse API Router:', function() {

  it('should return an express router instance', function() {
    autoResponseIndex.should.equal(routerStub);
  });

  describe('GET /api/auto-responses', function() {

    it('should route to autoResponse.controller.index', function() {
      routerStub.get
        .withArgs('/', 'autoResponseCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/auto-responses/:id', function() {

    it('should route to autoResponse.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'autoResponseCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/auto-responses', function() {

    it('should route to autoResponse.controller.create', function() {
      routerStub.post
        .withArgs('/', 'autoResponseCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/auto-responses/:id', function() {

    it('should route to autoResponse.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'autoResponseCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/auto-responses/:id', function() {

    it('should route to autoResponse.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'autoResponseCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/auto-responses/:id', function() {

    it('should route to autoResponse.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'autoResponseCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
