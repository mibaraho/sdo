/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/mercadolibre              ->  index
 * POST    /api/mercadolibre              ->  create
 * GET     /api/mercadolibre/:id          ->  show
 * PUT     /api/mercadolibre/:id          ->  update
 * DELETE  /api/mercadolibre/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {MarketplaceConnection} from '../../sqldb';
import {MercadolibreConnectionConfiguration} from '../../sqldb';
import config from '../../config/environment';
var rp = require('request-promise');
function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}


function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err)
    res.status(statusCode).send(err);
  };
}

// Gets a list of Mercadolibre configuration
export function configuration(req, res) {
  var mercadolibre = config.mercadolibre
  var _mercadolibre = _.map(mercadolibre.countries, function(item){
    var _item = _.pick(item, 'name', 'code', 'site', 'appId', 'authBaseUrl', 'redirectUri', 'callbackUrl', 'apiBaseUrl')
    _item.authUrl = item.authBaseUrl +'/authorization?response_type=code&client_id='+item.appId+'&redirect_uri='+item.redirectUri
    return _item;
  })
  res.json(_mercadolibre);
}

export function getAuthToken(req, res) {
  var code = req.query.code
  var mercadolibre = config.mercadolibre
  var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == req.params.country.toLowerCase()})
  var options = {
      method: 'POST',
      body: {},
      uri: _config.apiBaseUrl + '/oauth/token?grant_type=authorization_code&client_id='+_config.appId+'&client_secret='+_config.secretKey+'&code='+code+'&redirect_uri='+_config.redirectUri,
      json: true // Automatically parses the JSON string in the response
  };
  return rp(options)
    .then(function(result){
      var _mercadolibre_connection = {}
      _mercadolibre_connection.access_token = result.access_token
      _mercadolibre_connection.token_type = result.token_type
      _mercadolibre_connection.expires_in = result.expires_in
      _mercadolibre_connection.scope = result.scope
      _mercadolibre_connection.user_id = result.user_id
      _mercadolibre_connection.refresh_token = result.refresh_token
      _mercadolibre_connection.country = req.params.country
      _mercadolibre_connection.content = JSON.stringify(result)
      _mercadolibre_connection.pictureUrl = _config.pictureUrl
      _mercadolibre_connection.CreatedById = req.user._id
      _mercadolibre_connection.UpdatedById = req.user._id
      _mercadolibre_connection.MerchantId = req.params.id
      _mercadolibre_connection.auth_type = mercadolibre.auth_type
      _mercadolibre_connection.provider = mercadolibre.provider
      return MarketplaceConnection.create(_mercadolibre_connection)
    }).then(function(marketPlaceConnection){
      var _mercadolibre_connection_configuration = {}
      _mercadolibre_connection_configuration.CreatedById = req.user._id
      _mercadolibre_connection_configuration.UpdatedById = req.user._id
      _mercadolibre_connection_configuration.MerchantId = req.params.id
      _mercadolibre_connection_configuration.MarketplaceConnectionId = marketPlaceConnection._id
      return MercadolibreConnectionConfiguration.create(_mercadolibre_connection_configuration)
        .then(function(result){
          return marketPlaceConnection
        })
    })
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

}
export function conditions(req, res){
  res.json([{ id: 'new', name: 'MARKETPLACES.Mercadolibre.Conditions.New.Name' },{id: 'used', name: 'MARKETPLACES.Mercadolibre.Conditions.Used.Name' }]);
}
export function getCountry(req, res){
    var mercadolibre = config.mercadolibre
    var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == req.params.country.toLowerCase()})

    var options = {
        uri: _config.apiBaseUrl + '/countries/' + req.params.country.toUpperCase(),
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
      .then(function(result){
        return result
      })
      .then(respondWithResult(res, 201))
      .catch(handleError(res));
}
export function getSiteCategories(req, res){
    var mercadolibre = config.mercadolibre
    var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == req.params.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + '/sites/' + _config.site + '/categories',
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
      .then(respondWithResult(res, 201))
      .catch(handleError(res));
}
export function getSiteListingTypes(req, res){
    var mercadolibre = config.mercadolibre
    var _config = _.find(mercadolibre.countries, function(item){ return item.code.toLowerCase() == req.params.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + '/sites/' + _config.site + '/listing_types',
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
      .then(function(result){
        return result
      })
      .then(respondWithResult(res, 201))
      .catch(handleError(res));
}


// Gets a list of Mercadolibres
export function index(req, res) {
  Mercadolibre.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Mercadolibre from the DB
export function show(req, res) {
  Mercadolibre.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Mercadolibre in the DB
export function create(req, res) {
  Mercadolibre.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Mercadolibre in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Mercadolibre.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
