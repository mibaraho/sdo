'use strict';

var express = require('express');
var controller = require('./mercadolibre.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/mercadolibre/configuration', auth.isAuthenticated(), controller.configuration);
router.get('/m/:id/mercadolibre/:country/get-auth-token', auth.isAuthenticated(), controller.getAuthToken);
router.get('/mercadolibre/conditions', controller.conditions);
router.get('/mercadolibre/countries/:country', controller.getCountry);
router.get('/mercadolibre/:country/categories', controller.getSiteCategories);
router.get('/mercadolibre/:country/listing-types', controller.getSiteListingTypes);


module.exports = router;
