/**
 * MerchantUser model events
 */

'use strict';

import {EventEmitter} from 'events';
var MerchantUser = require('../../sqldb').MerchantUser;
var MerchantUserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MerchantUserEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  MerchantUser.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    MerchantUserEvents.emit(event + ':' + doc._id, doc);
    MerchantUserEvents.emit(event, doc);
    done(null);
  }
}

export default MerchantUserEvents;
