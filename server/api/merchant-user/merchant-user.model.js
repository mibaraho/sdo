'use strict';

export default function(sequelize, DataTypes) {
  var MerchantUser = sequelize.define('MerchantUser', {
    status: {
      allowNull: false,
      defaultValue: 'created',
      type: DataTypes.STRING
    },
    roles: DataTypes.STRING(2047)
  });

  return MerchantUser
}
