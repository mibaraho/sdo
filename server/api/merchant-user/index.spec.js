'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var merchantUserCtrlStub = {
  index: 'merchantUserCtrl.index',
  show: 'merchantUserCtrl.show',
  create: 'merchantUserCtrl.create',
  update: 'merchantUserCtrl.update',
  destroy: 'merchantUserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var merchantUserIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './merchant-user.controller': merchantUserCtrlStub
});

describe('MerchantUser API Router:', function() {

  it('should return an express router instance', function() {
    expect(merchantUserIndex).to.equal(routerStub);
  });

  describe('GET /api/merchant-users', function() {

    it('should route to merchantUser.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'merchantUserCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/merchant-users/:id', function() {

    it('should route to merchantUser.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'merchantUserCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/merchant-users', function() {

    it('should route to merchantUser.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'merchantUserCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/merchant-users/:id', function() {

    it('should route to merchantUser.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'merchantUserCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/merchant-users/:id', function() {

    it('should route to merchantUser.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'merchantUserCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/merchant-users/:id', function() {

    it('should route to merchantUser.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'merchantUserCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
