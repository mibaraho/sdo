'use strict';

var app = require('../..');
import request from 'supertest';

var newMerchantUser;

describe('MerchantUser API:', function() {

  describe('GET /api/merchant-users', function() {
    var merchantUsers;

    beforeEach(function(done) {
      request(app)
        .get('/api/merchant-users')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          merchantUsers = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(merchantUsers).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/merchant-users', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/merchant-users')
        .send({
          name: 'New MerchantUser',
          info: 'This is the brand new merchantUser!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMerchantUser = res.body;
          done();
        });
    });

    it('should respond with the newly created merchantUser', function() {
      expect(newMerchantUser.name).to.equal('New MerchantUser');
      expect(newMerchantUser.info).to.equal('This is the brand new merchantUser!!!');
    });

  });

  describe('GET /api/merchant-users/:id', function() {
    var merchantUser;

    beforeEach(function(done) {
      request(app)
        .get('/api/merchant-users/' + newMerchantUser._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          merchantUser = res.body;
          done();
        });
    });

    afterEach(function() {
      merchantUser = {};
    });

    it('should respond with the requested merchantUser', function() {
      expect(merchantUser.name).to.equal('New MerchantUser');
      expect(merchantUser.info).to.equal('This is the brand new merchantUser!!!');
    });

  });

  describe('PUT /api/merchant-users/:id', function() {
    var updatedMerchantUser;

    beforeEach(function(done) {
      request(app)
        .put('/api/merchant-users/' + newMerchantUser._id)
        .send({
          name: 'Updated MerchantUser',
          info: 'This is the updated merchantUser!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMerchantUser = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMerchantUser = {};
    });

    it('should respond with the updated merchantUser', function() {
      expect(updatedMerchantUser.name).to.equal('Updated MerchantUser');
      expect(updatedMerchantUser.info).to.equal('This is the updated merchantUser!!!');
    });

  });

  describe('DELETE /api/merchant-users/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/merchant-users/' + newMerchantUser._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when merchantUser does not exist', function(done) {
      request(app)
        .delete('/api/merchant-users/' + newMerchantUser._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
