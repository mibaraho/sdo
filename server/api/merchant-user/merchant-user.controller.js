/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/merchant-users              ->  index
 * POST    /api/merchant-users              ->  create
 * GET     /api/merchant-users/:id          ->  show
 * PUT     /api/merchant-users/:id          ->  update
 * DELETE  /api/merchant-users/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {MerchantUser} from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of MerchantUsers
export function index(req, res) {
  MerchantUser.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single MerchantUser from the DB
export function show(req, res) {
  MerchantUser.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new MerchantUser in the DB
export function create(req, res) {
  MerchantUser.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing MerchantUser in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  MerchantUser.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MerchantUser from the DB
export function destroy(req, res) {
  MerchantUser.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
