'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var marketplaceCtrlStub = {
  index: 'marketplaceCtrl.index',
  show: 'marketplaceCtrl.show',
  create: 'marketplaceCtrl.create',
  update: 'marketplaceCtrl.update',
  destroy: 'marketplaceCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var marketplaceIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './marketplace.controller': marketplaceCtrlStub
});

describe('Marketplace API Router:', function() {

  it('should return an express router instance', function() {
    expect(marketplaceIndex).to.equal(routerStub);
  });

  describe('GET /api/marketplaces', function() {

    it('should route to marketplace.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'marketplaceCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/marketplaces/:id', function() {

    it('should route to marketplace.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'marketplaceCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/marketplaces', function() {

    it('should route to marketplace.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'marketplaceCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/marketplaces/:id', function() {

    it('should route to marketplace.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'marketplaceCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/marketplaces/:id', function() {

    it('should route to marketplace.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'marketplaceCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/marketplaces/:id', function() {

    it('should route to marketplace.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'marketplaceCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
