'use strict';

export default function(sequelize, DataTypes) {
  var Marketplace = sequelize.define('Marketplace', {
    _id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    status: {
      allowNull: false,
      defaultValue: 'created',
      type: DataTypes.STRING
    }
  });

  Marketplace.Items = {
    MERCADOLIBRE: 'Mercadolibre',
    LINIO: 'Linio',
    DAFITI: 'Dafiti'
  }

  return Marketplace;
}
