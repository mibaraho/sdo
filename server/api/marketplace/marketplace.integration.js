'use strict';

var app = require('../..');
import request from 'supertest';

var newMarketplace;

describe('Marketplace API:', function() {

  describe('GET /api/marketplaces', function() {
    var marketplaces;

    beforeEach(function(done) {
      request(app)
        .get('/api/marketplaces')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marketplaces = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(marketplaces).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/marketplaces', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/marketplaces')
        .send({
          name: 'New Marketplace',
          info: 'This is the brand new marketplace!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMarketplace = res.body;
          done();
        });
    });

    it('should respond with the newly created marketplace', function() {
      expect(newMarketplace.name).to.equal('New Marketplace');
      expect(newMarketplace.info).to.equal('This is the brand new marketplace!!!');
    });

  });

  describe('GET /api/marketplaces/:id', function() {
    var marketplace;

    beforeEach(function(done) {
      request(app)
        .get('/api/marketplaces/' + newMarketplace._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marketplace = res.body;
          done();
        });
    });

    afterEach(function() {
      marketplace = {};
    });

    it('should respond with the requested marketplace', function() {
      expect(marketplace.name).to.equal('New Marketplace');
      expect(marketplace.info).to.equal('This is the brand new marketplace!!!');
    });

  });

  describe('PUT /api/marketplaces/:id', function() {
    var updatedMarketplace;

    beforeEach(function(done) {
      request(app)
        .put('/api/marketplaces/' + newMarketplace._id)
        .send({
          name: 'Updated Marketplace',
          info: 'This is the updated marketplace!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMarketplace = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMarketplace = {};
    });

    it('should respond with the updated marketplace', function() {
      expect(updatedMarketplace.name).to.equal('Updated Marketplace');
      expect(updatedMarketplace.info).to.equal('This is the updated marketplace!!!');
    });

  });

  describe('DELETE /api/marketplaces/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/marketplaces/' + newMarketplace._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when marketplace does not exist', function(done) {
      request(app)
        .delete('/api/marketplaces/' + newMarketplace._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
