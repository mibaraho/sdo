'use strict';

var express = require('express');
var controller = require('./marketplace-connection.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/m/:id/marketplace-connections/p/:page', auth.isAuthenticated(), controller.index);
router.get('/m/:id/marketplace-connections', auth.isAuthenticated(), controller.index);
router.get('/marketplace-connections/:id', auth.isAuthenticated(), controller.show);
router.put('/marketplace-connections/:id', auth.isAuthenticated(), controller.update);
router.patch('/marketplace-connections/:id', auth.isAuthenticated(), controller.update);
router.delete('/marketplace-connections/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
