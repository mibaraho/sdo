/**
 * MarketplaceConnection model events
 */

'use strict';

import {EventEmitter} from 'events';
var MarketplaceConnection = require('../../sqldb').MarketplaceConnection;
var MarketplaceConnectionEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MarketplaceConnectionEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  MarketplaceConnection.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    MarketplaceConnectionEvents.emit(event + ':' + doc._id, doc);
    MarketplaceConnectionEvents.emit(event, doc);
    done(null);
  }
}

export default MarketplaceConnectionEvents;
