'use strict';

var app = require('../..');
import request from 'supertest';

var newMarketplaceConnection;

describe('MarketplaceConnection API:', function() {

  describe('GET /api/marketplace-connections', function() {
    var marketplaceConnections;

    beforeEach(function(done) {
      request(app)
        .get('/api/marketplace-connections')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marketplaceConnections = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(marketplaceConnections).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/marketplace-connections', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/marketplace-connections')
        .send({
          name: 'New MarketplaceConnection',
          info: 'This is the brand new marketplaceConnection!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMarketplaceConnection = res.body;
          done();
        });
    });

    it('should respond with the newly created marketplaceConnection', function() {
      expect(newMarketplaceConnection.name).to.equal('New MarketplaceConnection');
      expect(newMarketplaceConnection.info).to.equal('This is the brand new marketplaceConnection!!!');
    });

  });

  describe('GET /api/marketplace-connections/:id', function() {
    var marketplaceConnection;

    beforeEach(function(done) {
      request(app)
        .get('/api/marketplace-connections/' + newMarketplaceConnection._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          marketplaceConnection = res.body;
          done();
        });
    });

    afterEach(function() {
      marketplaceConnection = {};
    });

    it('should respond with the requested marketplaceConnection', function() {
      expect(marketplaceConnection.name).to.equal('New MarketplaceConnection');
      expect(marketplaceConnection.info).to.equal('This is the brand new marketplaceConnection!!!');
    });

  });

  describe('PUT /api/marketplace-connections/:id', function() {
    var updatedMarketplaceConnection;

    beforeEach(function(done) {
      request(app)
        .put('/api/marketplace-connections/' + newMarketplaceConnection._id)
        .send({
          name: 'Updated MarketplaceConnection',
          info: 'This is the updated marketplaceConnection!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMarketplaceConnection = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMarketplaceConnection = {};
    });

    it('should respond with the updated marketplaceConnection', function() {
      expect(updatedMarketplaceConnection.name).to.equal('Updated MarketplaceConnection');
      expect(updatedMarketplaceConnection.info).to.equal('This is the updated marketplaceConnection!!!');
    });

  });

  describe('DELETE /api/marketplace-connections/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/marketplace-connections/' + newMarketplaceConnection._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when marketplaceConnection does not exist', function(done) {
      request(app)
        .delete('/api/marketplace-connections/' + newMarketplaceConnection._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
