'use strict';

export default function(sequelize, DataTypes) {
  var Marketplace = sequelize.import('../marketplace/marketplace.model');
  var MarketplaceConnection = sequelize.define('MarketplaceConnection', {
    _id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: DataTypes.STRING(2047),
    provider: DataTypes.STRING,//Mercadolibre, Linio, Amazon, etc.
    auth_type: DataTypes.STRING,//OAuth, Api key, etc
    api_username:  DataTypes.STRING(2047),
    api_user_id:  DataTypes.STRING(2047),
    api_key:  DataTypes.TEXT,//Must be stored encrypted
    access_token: DataTypes.STRING,
    token_type: DataTypes.STRING,
    expires_in: DataTypes.INTEGER,
    scope: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    refresh_token: DataTypes.STRING,
    country: DataTypes.STRING,
    pictureUrl: DataTypes.STRING(2047),
    content: DataTypes.TEXT,
    name: DataTypes.STRING(2047),
    synchronizationProcessStatus: {
      allowNull: false,
      defaultValue: 'paused',
      type: DataTypes.STRING
    },
    synchronizationStatus: {
      allowNull: false,
      defaultValue: 'not-synchronized',
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      defaultValue: 'created',
      type: DataTypes.STRING
    }
  }, {

    /**
     * Virtual Getters
     */
    getterMethods: {
      _name: function() {
        if(this.name){ return this.name }
        if(this.provider === Marketplace.Items.MERCADOLIBRE.toLowerCase()) { return Marketplace.Items.MERCADOLIBRE}
        if(this.provider === Marketplace.Items.LINIO.toLowerCase()) { return Marketplace.Items.LINIO}
        if(this.provider === Marketplace.Items.DAFITI.toLowerCase()) { return Marketplace.Items.DAFITI}
        return ''
      },

    }});

  var User = sequelize.import('../user/user.model');
  MarketplaceConnection.belongsTo(User, { as: 'CreatedBy' });
  MarketplaceConnection.belongsTo(User, { as: 'UpdatedBy' });

  MarketplaceConnection.SynchronizationStatus = {
    NOT_SYNCHRONIZED: 'not-synchronized',
    SYNCHRONIZING: 'synchronizing',
    SYNCHRONIZED: 'synchronized',
  }
  MarketplaceConnection.SynchronizationProcessStatus = {
    PAUSED: 'paused',
    STARTED: 'started'
  }

  return MarketplaceConnection;
}
