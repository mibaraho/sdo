/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/marketplace-connections              ->  index
 * POST    /api/marketplace-connections              ->  create
 * GET     /api/marketplace-connections/:id          ->  show
 * PUT     /api/marketplace-connections/:id          ->  update
 * DELETE  /api/marketplace-connections/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {MarketplaceConnection} from '../../sqldb';
import {MarketplaceConnectionStockUsagePolicyItem} from '../../sqldb';
import * as pagination from '../../utils/pagination';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(req, res) {
  return function(entity) {
    if (entity) {
      var update = { status: 'deleted', UpdatedById: req.user._id };
      return entity.update(update)
        .then(function(result) {
          res.status(200).json(result)
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err)
    res.status(statusCode).send(err);
  };
}

// Gets a list of marketplace-connections
export function index(req, res) {
  var _where = {
    status: 'created',
    MerchantId: req.params.id
  }
  return MarketplaceConnection.count({
     where: _where
   }).then(function(count){
      var _pagination = {
        limit: count
      }
      if(req.params.page){
        _pagination = pagination.paginate(req.params.page, count)
      }
      return MarketplaceConnection.findAll({
        where: _where,
        order: [
          ['createdAt', 'DESC']
        ],
        offset: _pagination.offset,
        limit: _pagination.limit
      }).then(function(result){
          var resultWrapper = {}
          resultWrapper.entries = result;
          resultWrapper.pagination = _pagination;
          return resultWrapper
        })
    .then(respondWithResult(res))
    .catch(handleError(res));
    });
}

// Gets a single MarketplaceConnection from the DB
export function show(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    },
    attributes: ['_id', 'name', 'provider', 'auth_type', 'api_user_id', 'api_user_id', 'country', 'synchronizationProcessStatus', 'synchronizationStatus', 'status'],
    include: [{
      model: MarketplaceConnectionStockUsagePolicyItem
    }]
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new MarketplaceConnection in the DB
export function create(req, res) {
  var _marketplace_connection = _.pick(req.body, 'name', 'description');
  _marketplace_connection.CreatedById = req.user._id;
  _marketplace_connection.UpdatedById = req.user._id;
  _marketplace_connection.MerchantId = req.params.id;
  MarketplaceConnection.create(_marketplace_connection)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing MarketplaceConnection in the DB
export function update(req, res) {
  var _marketplace_connection = _.pick(req.body, 'name', 'description');
  _marketplace_connection.UpdatedById = req.user._id;
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(_marketplace_connection))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MarketplaceConnection from the DB
export function destroy(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(req, res))
    .catch(handleError(res));
}
