'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var marketplaceConnectionCtrlStub = {
  index: 'marketplaceConnectionCtrl.index',
  show: 'marketplaceConnectionCtrl.show',
  create: 'marketplaceConnectionCtrl.create',
  update: 'marketplaceConnectionCtrl.update',
  destroy: 'marketplaceConnectionCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var marketplaceConnectionIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './marketplace-connection.controller': marketplaceConnectionCtrlStub
});

describe('MarketplaceConnection API Router:', function() {

  it('should return an express router instance', function() {
    expect(marketplaceConnectionIndex).to.equal(routerStub);
  });

  describe('GET /api/marketplace-connections', function() {

    it('should route to marketplaceConnection.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'marketplaceConnectionCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/marketplace-connections/:id', function() {

    it('should route to marketplaceConnection.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'marketplaceConnectionCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/marketplace-connections', function() {

    it('should route to marketplaceConnection.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'marketplaceConnectionCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/marketplace-connections/:id', function() {

    it('should route to marketplaceConnection.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'marketplaceConnectionCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/marketplace-connections/:id', function() {

    it('should route to marketplaceConnection.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'marketplaceConnectionCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/marketplace-connections/:id', function() {

    it('should route to marketplaceConnection.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'marketplaceConnectionCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
