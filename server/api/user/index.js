'use strict';

import {Router} from 'express';
import * as controller from './user.controller';
import * as auth from '../../auth/auth.service';

var router = new Router();

router.get('/m/:id/users', auth.isAuthenticated(), controller.index);
//router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/users/me', auth.isAuthenticated(), controller.me);
router.get('/m/:id/users/me/points-of-sale', auth.isAuthenticated(), controller.pointsOfSale);
router.get('/m/:id/users/:userId/points-of-sale', auth.isAuthenticated(), controller.pointsOfSale);
router.put('/users/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/m/:id/users/:userId/permissions/general', controller.permissions);
router.get('/m/:id/users/:userId/permissions/general/filter/:filter', controller.permissions);
router.get('/users/:id', auth.isAuthenticated(), controller.show);
router.post('/users', controller.selfServiceCreate);

export default router;
