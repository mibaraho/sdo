'use strict';

import _ from 'lodash';
import {Location} from '../../sqldb';
import {User} from '../../sqldb';
import {Merchant} from '../../sqldb';
import {MerchantUser} from '../../sqldb';
import passport from 'passport';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';
import {sequelize} from '../../sqldb';
import * as mailer from '../../components/background-tasks/emails/queue';
import i18n  from 'i18n';
var Promise = require('promise');


function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    console.log(err)
    res.status(statusCode).json(err);
  }
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err)
    res.status(statusCode).send(err);
  };
}
function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}
function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

export function index(req, res) {

  var _where = {
      _id: req.params.id
    }
  return Merchant.find({
    where: _where,
    include:[{
      model: User,
      attributes: ['_id', 'name', 'email', 'role','provider', 'createdAt']
    }]
  })
  .then(handleEntityNotFound(res))
  .then(function(result){
    res.status(200).json(result.Users);
  })
  .catch(handleError(res));
}


/**
 * Creates a new user when the user is signing up by himself
 */
export function selfServiceCreate(req, res, next) {
  var _user = _.pick(req.body, 'name', 'email', 'password');
  _user.provider = 'local';
  return sequelize.transaction(function(_transaction){
    return User.create(_user, {transaction: _transaction})
      .then(initializeBundle(req, _transaction))
      .then(createMerchant(req, _transaction))
      .then(associateMerchantWithUser(req, _transaction))
      .then(signUserToken(req, _transaction))
      .then(respondWithResult(res))
      .catch(validationError(res))
      .catch(handleError(res));
  })
}

function initializeBundle(req, _transaction){
  return function(product){
    var bundle = {} //The bundle object will collect result while going through promises
    bundle.user = product;
    return bundle
  }
}
function findMerchantLocation(req, _transaction){
  return function(bundle){
    var locationId = req.body.merchant.LocationId
    return Location.find({
      where: {
        _id: locationId
      }
    })
    .then(function(result){
      bundle.location = result
      return bundle
    })

  }
}
function createMerchant(req, _transaction){
  return function(bundle){
    var _merchant = {};
    _merchant.name = req.body.merchant.name
    _merchant.LocationId = bundle.location ? bundle.location._id : null
    _merchant.MerchantActivityId = req.body.merchant.MerchantActivityId
    _merchant.phoneNumber = req.body.merchant.phoneNumber
    return Merchant.create(_merchant, {transaction: _transaction})
    .then(function(result){
      bundle.merchant = result
      return bundle
    })
  }
}
function associateMerchantWithUser(req, _transaction){
  return function(bundle){
    return bundle.user.addMerchant(bundle.merchant, { roles: 'owner', transaction: _transaction})
      .then(function(result){
        return bundle
      })
  }
}
function initializeMerchantConfigurationAssistant(req, _transaction){
  return function(bundle){
    return MerchantConfigurationProcess.find({
      where: {
        status: 'created',
        code: 'initial-merchant-configuration'
      },
      include: [{
        model: MerchantConfigurationProcessStep,
        where: {
          status: 'created'
        }
      }]
    })
    .then(function(result){
      bundle.merchantConfigurationProcess = result
      return bundle
    })
    .then(function(bundle){
      if(!bundle.merchantConfigurationProcess){return bundle;}
      var _merchant_configuration_process_instance = {}
      _merchant_configuration_process_instance.MerchantId = bundle.merchant._id
      _merchant_configuration_process_instance.MerchantConfigurationProcessId = bundle.merchantConfigurationProcess._id
      _merchant_configuration_process_instance.CreatedById = bundle.user._id
      _merchant_configuration_process_instance.UpdatedById = bundle.user._id
      return MerchantConfigurationProcessInstance.create(_merchant_configuration_process_instance, {transaction: _transaction})
        .then(function(result){
          bundle.merchantConfigurationProcessInstance = result
          return bundle
        })
    })
    .then(function(bundle){
      if(!bundle.merchantConfigurationProcess){return bundle;}
      var promises = []
      _.each(bundle.merchantConfigurationProcess.MerchantConfigurationProcessSteps, function(item){
        var _merchant_configuration_process_instance_step = {}
        _merchant_configuration_process_instance_step.MerchantId = bundle.merchant._id
        _merchant_configuration_process_instance_step.MerchantConfigurationProcessInstanceId = bundle.merchantConfigurationProcessInstance._id
        _merchant_configuration_process_instance_step.MerchantConfigurationProcessStepId = item._id
        _merchant_configuration_process_instance_step.code = item.code
        _merchant_configuration_process_instance_step.MerchantId = bundle.merchantConfigurationProcessInstance.MerchantId
        _merchant_configuration_process_instance_step.position = item.position
        _merchant_configuration_process_instance_step.CreatedById = bundle.user._id
        _merchant_configuration_process_instance_step.UpdatedById = bundle.user._id
        var promise = MerchantConfigurationProcessInstanceStep.create(_merchant_configuration_process_instance_step, {transaction: _transaction})
        promises.push(promise)
      })
    return Promise.all(promises)
      .then(function(result){
        return bundle;
      });

    })
  }
}

function sendWelcomeEmail(req){
  return function(bundle){
    mailer.queueEmailToUserWhenCreatesAnAccount(bundle.user)
    return bundle
  }
}

function initializeDefaultCurrencies(req, _transaction){
  return function(bundle){
    return PlatformCurrency.findAll({
      where: {
        status: 'created'
      }
    }).then(function(platformCurrencies){
      var promises = []
      _.each(platformCurrencies, function(item){
        var _currency = {}
        _currency.PlatformCurrencyId = item._id
        _currency.CreatedById = bundle.user._id
        _currency.UpdatedById = bundle.user._id
        _currency.MerchantId = bundle.merchant._id
        var promise = Currency.create(_currency, {transaction: _transaction})
        promises.push(promise)
      })
    return Promise.all(promises)
      .then(function(result){
        bundle.currencies = result
        return bundle;
      });

    })
  }
}
function initializeDefaultPriceLists(req, _transaction){
  return function(bundle){
    var promises = []
    _.each(bundle.currencies, function(item){
      var _product_price_list = {}
      _product_price_list.CurrencyId = item._id;
      _product_price_list.MerchantId = bundle.merchant._id
      _product_price_list.CreatedById = bundle.user._id;
      _product_price_list.UpdatedById = bundle.user._id;
      _product_price_list.isDefault = true
      _product_price_list.name = i18n.__('PRODUCT_PRICE_LISTS.Default_product_price_list');
      var promise = ProductPriceList.create(_product_price_list, {transaction: _transaction})
      promises.push(promise)
    })
    return Promise.all(promises)
      .then(function(result){
        bundle.productPriceLists = result
        return bundle;
      });

  }
}
function initializeDefaultStore(req, _transaction){
  return function(bundle){
    var _store = {}
    _store.MerchantId = bundle.merchant._id
    _store.CreatedById = bundle.user._id;
    _store.UpdatedById = bundle.user._id;
    _store.name = i18n.__('STORES_AND_WAREHOUSES.Store.Initial');
    _store.type = 'store'
    return Warehouse.create(_store, {transaction: _transaction})
      .then(function(result){
        bundle.store = result
        return bundle
      })
  }
}
function initializeDefaultWarehouse(req, _transaction){
  return function(bundle){
    var _warehouse = {}
    _warehouse.MerchantId = bundle.merchant._id
    _warehouse.CreatedById = bundle.user._id;
    _warehouse.UpdatedById = bundle.user._id;
    _warehouse.name = i18n.__('STORES_AND_WAREHOUSES.Warehouse.Initial');
    _warehouse.type = 'warehouse'
    return Warehouse.create(_warehouse, {transaction: _transaction})
      .then(function(result){
        bundle.store = result
        return bundle
      })
  }
}
function signUserToken(req){
  return function(bundle){
    var result = {}
    result.user = bundle.user
    result.merchant = bundle.merchant
      result.token = jwt.sign({ _id: bundle.user._id }, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
    return result;
  }
}
/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  User.find({
    where: {
      _id: userId
    },
    attributes: ['_id', 'name', 'email', 'role','provider', 'createdAt']
  })
    .then(user => {
      if (!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Get a single user
 */
export function permissions(req, res) {
  return Merchant.find({//First, let's check if the user exists and what's his role in the specific merchant
    where: {
      _id: req.params.id,
      status: 'created'
    },
    include: [{
      model: User,
      where: {
        _id: req.params.userId
      },
      status:'created'
    }]
  })
  .then(function(merchant){
    if(!merchant){
      res.status(200).json({});//If there is no merchantUser, we return an empty object (not permissions at all)
    } else {
      var bundle = {}
      bundle.merchantUser = _.first(merchant.Users).MerchantUser
      return bundle
    }
  })
  .then(function(bundle){
    if(!bundle.merchantUser) { return; }
    return Action.findAll({
      attributes: ['_id', 'code', 'name', 'description', 'position', 'scope'],
      where: {
        status: 'created',
        code: {
          $ne: null
        }
      },
      include: [{
        model: ActionGroup,
        attributes: ['_id', 'name'],
        },{
        model: ActionInUserGroup,
        where: {
          status: 'created'
        },
        required: false,
        include: [{
          model: UserGroup,
          where: {
            status: 'created'
          },
          required: false,
          where: {
            MerchantId: req.params.id
          },
          include: [{
            model: UserInGroup,
            where: {
              status: 'created'
            },
            required: false,
            include: [{
              model: User,
              attributes: ['_id', 'name', 'email', 'role', 'provider'],
              required: false,
              where: {
                status: 'created',
                _id: req.params.userId
              }
            }]
          }]
        }]
      }]
    })
    .then(function(result){
      bundle.result = result;
      return bundle
    })
  }).then(function(bundle){
    //return res.status(200).json({result: bundle.result});
    var _result = _.map(bundle.result, function(item){
      var _item = {}
      _item.code = item.code
      if(bundle.merchantUser.roles == 'owner'){//Owners can do everything
        _item.allowed = true
        return _item
      } else if(!item.ActionInUserGroups.length){//If the action is not associated to at least one group, it can not be done
        _item.allowed = false
        return _item
      } else if(_.find(item.ActionInUserGroups, function(item){ return !item.UserGroup.UserInGroups.length})){//If non of the groups has users, we return false
        _item.allowed = false
        return _item
      } else if(_.find(_.filter(item.ActionInUserGroups, function(item){ return !item.UserGroup.UserInGroups.length})), function(item){ return item.User }){//If the user shows up, whe return true
        _item.allowed = true
        return _item
      }

    })
    var _result = _.keyBy(_result, function(item){
      return item.code
    })
    var __result = _.mapValues(_result, 'allowed');
    return res.status(200).json(__result);
  })
}


/**
 * Get the user points of sale
 */
export function pointsOfSale(req, res, next) {

  var userId = req.params.userId ? req.params.userId : req.user._id
  var merchantId = req.params.id

  return Merchant.find({//First, let's check if the user exists and what's his role in the specific merchant
    where: {
      _id: req.params.id,
      status: 'created'
    },
    include: [{
      model: User,
      where: {
        _id: userId
      },
      status:'created'
    }]
  })
  .then(handleEntityNotFound(res))
  .then(function(merchant){
    if(!merchant){
      return;
    } else {
      var bundle = {}
      bundle.merchantUser = _.first(merchant.Users).MerchantUser
      return bundle
    }
  })
  .then(function(bundle){
    if(!bundle.merchantUser) { return; }
    return Action.findAll({
      attributes: ['_id', 'code', 'name', 'description', 'position', 'scope'],
      where: {
        status: 'created',
        code: '_ACTION_SALE_EVERYWHERE_'
      },
      include: [{
        model: ActionGroup,
        attributes: ['_id', 'name'],
        },{
        model: ActionInUserGroup,
        where: {
          status: 'created'
        },
        required: false,
        include: [{
          model: UserGroup,
          where: {
            status: 'created'
          },
          required: false,
          where: {
            MerchantId: req.params.id
          },
          include: [{
            model: UserInGroup,
            where: {
              status: 'created'
            },
            required: false,
            include: [{
              model: User,
              attributes: ['_id', 'name', 'email', 'role', 'provider'],
              required: false,
              where: {
                status: 'created',
                _id: userId
              }
            }]
          }]
        }]
      }]
    })
    .then(function(result){
      bundle.result = result;
      return bundle
    })
  }).then(function(bundle){
      if(bundle.merchantUser.roles == 'owner' || (bundle.result.length && _.first(bundle.result).ActionInUserGroups.length)){
        bundle.returnAll = true;
      } else {
        bundle.returnAll = false;
      }
    return bundle
  }).then(function(bundle){
    if(bundle.returnAll){
      Warehouse.findAll({
        where: {
          status: 'created',
          MerchantId: merchantId,
          type: 'store'
        }
      }).then(function(result){
        res.status(200).json(_.map(result, function(item){ return item.dataValues; }));
      })
    } else {
      return res.status(200).json([])
    }
  })
}


/**
 * Change a users password
 */
export function changePassword(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return User.find({
    where: {
      _id: userId
    }
  })
    .then(user => {
      if (user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;

  return User.find({
    where: {
      _id: userId
    },
    attributes: [
      '_id',
      'name',
      'email',
      'role',
      'provider',
      'createdAt'
    ], include: [{
    model: Merchant,
      through: {
        attributes: ['roles']
      }
  }]
  })
    .then(user => { // don't ever give out the password or salt
      if (!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
export function authCallback(req, res, next) {
  res.redirect('/');
}
