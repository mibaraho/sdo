'use strict';

export default function(sequelize, DataTypes) {
  var Merchant = sequelize.define('Merchant', {
    _id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: DataTypes.STRING(2047),
    description: DataTypes.TEXT,
    code: DataTypes.STRING(2047),
    address: DataTypes.TEXT,
    zipCode: DataTypes.TEXT,
    activity: DataTypes.STRING(2047),
    taxId: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    phoneNumber: DataTypes.STRING(2047),
    status: {
      allowNull: false,
      defaultValue: 'created',
      type: DataTypes.STRING
    }
  });

  var User =  sequelize.import('../user/user.model');
  var MerchantUser =  sequelize.import('../merchant-user/merchant-user.model');
  Merchant.belongsToMany(User, { through: MerchantUser });
  User.belongsToMany(Merchant, { through: MerchantUser });

  var MarketplaceConnection =  sequelize.import('../marketplace-connection/marketplace-connection.model');
  Merchant.hasMany(MarketplaceConnection);

  return Merchant;
}
