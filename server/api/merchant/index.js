'use strict';

var express = require('express');
var controller = require('./merchant.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/merchants/:id', controller.show);
router.put('/merchants/:id', auth.isAuthenticated(), controller.update);
/*router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);*/

module.exports = router;
