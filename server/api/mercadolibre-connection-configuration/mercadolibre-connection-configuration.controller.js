/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/mercadolibre-connection-configurations              ->  index
 * POST    /api/mercadolibre-connection-configurations              ->  create
 * GET     /api/mercadolibre-connection-configurations/:id          ->  show
 * PUT     /api/mercadolibre-connection-configurations/:id          ->  update
 * DELETE  /api/mercadolibre-connection-configurations/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {MercadolibreConnectionConfiguration} from '../../sqldb';
import * as pagination from '../../utils/pagination';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(req, res) {
  return function(entity) {
    if (entity) {
      var update = { status: 'deleted', UpdatedById: req.user._id };
      return entity.update(update)
        .then(function(result) {
          res.status(200).json(result)
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of MercadolibreConnectionConfigurations
export function index(req, res) {
  var _where = {
    status: 'created',
    MerchantId: req.params.id
  }
  MercadolibreConnectionConfiguration.count({
     where: _where
   }).then(function(count){
      var _pagination = {
        limit: count
      }
      if(req.params.page){
        _pagination = pagination.paginate(req.params.page, count)
      }
      MercadolibreConnectionConfiguration.findAll({
        where: _where,
        order: [
          ['createdAt', 'DESC']
        ],
        offset: _pagination.offset,
        limit: _pagination.limit
      }).then(function(result){
          var resultWrapper = {}
          resultWrapper.entries = result;
          resultWrapper.pagination = _pagination;
          return resultWrapper
        })
    .then(respondWithResult(res))
    .catch(handleError(res));
    });
}

// Gets a single MercadolibreConnectionConfiguration from the DB
export function show(req, res) {
  MercadolibreConnectionConfiguration.find({
    where: {
      MarketplaceConnectionId: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing MercadolibreConnectionConfiguration in the DB
export function update(req, res) {
  var _mercadolibre_connection_configuration = _.pick(req.body, 'categoryMappingType', 'synchronizationScope', 'ListingTypeId', 'ProductConditionId', 'ProductPriceListId');
  _mercadolibre_connection_configuration.UpdatedById = req.user._id;
  MercadolibreConnectionConfiguration.find({
    where: {
      MarketplaceConnectionId: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(_mercadolibre_connection_configuration))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MercadolibreConnectionConfiguration from the DB
export function destroy(req, res) {
  MercadolibreConnectionConfiguration.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(req, res))
    .catch(handleError(res));
}
