'use strict';

var app = require('../..');
import request from 'supertest';

var newMercadolibreConnectionConfiguration;

describe('MercadolibreConnectionConfiguration API:', function() {

  describe('GET /api/mercadolibre-connection-configurations', function() {
    var mercadolibreConnectionConfigurations;

    beforeEach(function(done) {
      request(app)
        .get('/api/mercadolibre-connection-configurations')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mercadolibreConnectionConfigurations = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(mercadolibreConnectionConfigurations).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/mercadolibre-connection-configurations', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/mercadolibre-connection-configurations')
        .send({
          name: 'New MercadolibreConnectionConfiguration',
          info: 'This is the brand new mercadolibreConnectionConfiguration!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMercadolibreConnectionConfiguration = res.body;
          done();
        });
    });

    it('should respond with the newly created mercadolibreConnectionConfiguration', function() {
      expect(newMercadolibreConnectionConfiguration.name).to.equal('New MercadolibreConnectionConfiguration');
      expect(newMercadolibreConnectionConfiguration.info).to.equal('This is the brand new mercadolibreConnectionConfiguration!!!');
    });

  });

  describe('GET /api/mercadolibre-connection-configurations/:id', function() {
    var mercadolibreConnectionConfiguration;

    beforeEach(function(done) {
      request(app)
        .get('/api/mercadolibre-connection-configurations/' + newMercadolibreConnectionConfiguration._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mercadolibreConnectionConfiguration = res.body;
          done();
        });
    });

    afterEach(function() {
      mercadolibreConnectionConfiguration = {};
    });

    it('should respond with the requested mercadolibreConnectionConfiguration', function() {
      expect(mercadolibreConnectionConfiguration.name).to.equal('New MercadolibreConnectionConfiguration');
      expect(mercadolibreConnectionConfiguration.info).to.equal('This is the brand new mercadolibreConnectionConfiguration!!!');
    });

  });

  describe('PUT /api/mercadolibre-connection-configurations/:id', function() {
    var updatedMercadolibreConnectionConfiguration;

    beforeEach(function(done) {
      request(app)
        .put('/api/mercadolibre-connection-configurations/' + newMercadolibreConnectionConfiguration._id)
        .send({
          name: 'Updated MercadolibreConnectionConfiguration',
          info: 'This is the updated mercadolibreConnectionConfiguration!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMercadolibreConnectionConfiguration = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMercadolibreConnectionConfiguration = {};
    });

    it('should respond with the updated mercadolibreConnectionConfiguration', function() {
      expect(updatedMercadolibreConnectionConfiguration.name).to.equal('Updated MercadolibreConnectionConfiguration');
      expect(updatedMercadolibreConnectionConfiguration.info).to.equal('This is the updated mercadolibreConnectionConfiguration!!!');
    });

  });

  describe('DELETE /api/mercadolibre-connection-configurations/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/mercadolibre-connection-configurations/' + newMercadolibreConnectionConfiguration._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when mercadolibreConnectionConfiguration does not exist', function(done) {
      request(app)
        .delete('/api/mercadolibre-connection-configurations/' + newMercadolibreConnectionConfiguration._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
