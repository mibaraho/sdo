/**
 * MercadolibreConnectionConfiguration model events
 */

'use strict';

import {EventEmitter} from 'events';
var MercadolibreConnectionConfiguration = require('../../sqldb').MercadolibreConnectionConfiguration;
var MercadolibreConnectionConfigurationEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MercadolibreConnectionConfigurationEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  MercadolibreConnectionConfiguration.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    MercadolibreConnectionConfigurationEvents.emit(event + ':' + doc._id, doc);
    MercadolibreConnectionConfigurationEvents.emit(event, doc);
    done(null);
  }
}

export default MercadolibreConnectionConfigurationEvents;
