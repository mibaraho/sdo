'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var mercadolibreConnectionConfigurationCtrlStub = {
  index: 'mercadolibreConnectionConfigurationCtrl.index',
  show: 'mercadolibreConnectionConfigurationCtrl.show',
  create: 'mercadolibreConnectionConfigurationCtrl.create',
  update: 'mercadolibreConnectionConfigurationCtrl.update',
  destroy: 'mercadolibreConnectionConfigurationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var mercadolibreConnectionConfigurationIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './mercadolibre-connection-configuration.controller': mercadolibreConnectionConfigurationCtrlStub
});

describe('MercadolibreConnectionConfiguration API Router:', function() {

  it('should return an express router instance', function() {
    expect(mercadolibreConnectionConfigurationIndex).to.equal(routerStub);
  });

  describe('GET /api/mercadolibre-connection-configurations', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'mercadolibreConnectionConfigurationCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/mercadolibre-connection-configurations/:id', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'mercadolibreConnectionConfigurationCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/mercadolibre-connection-configurations', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'mercadolibreConnectionConfigurationCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/mercadolibre-connection-configurations/:id', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'mercadolibreConnectionConfigurationCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/mercadolibre-connection-configurations/:id', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'mercadolibreConnectionConfigurationCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/mercadolibre-connection-configurations/:id', function() {

    it('should route to mercadolibreConnectionConfiguration.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'mercadolibreConnectionConfigurationCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
