'use strict';

var express = require('express');
var controller = require('./mercadolibre-connection-configuration.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/mercadolibre-connections/:id/configuration', auth.isAuthenticated(), controller.show);
router.put('/mercadolibre-connections/:id/configuration', auth.isAuthenticated(), controller.update);

module.exports = router;
