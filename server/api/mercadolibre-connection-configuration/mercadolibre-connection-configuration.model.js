'use strict';

export default function(sequelize, DataTypes) {
  var MercadolibreConnectionConfiguration = sequelize.define('MercadolibreConnectionConfiguration', {
    _id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    categoryMappingType: {
      allowNull: false,
      defaultValue: 'automatic',
      type: DataTypes.STRING
    },
    synchronizationScope: {
      allowNull: false,
      defaultValue: 'all',
      type: DataTypes.STRING
    },
    ListingTypeId: DataTypes.STRING,
    ProductConditionId: {
      allowNull: false,
      defaultValue: 'new',
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      defaultValue: 'created',
      type: DataTypes.STRING
    }
  });

  var User = sequelize.import('../user/user.model');
  MercadolibreConnectionConfiguration.belongsTo(User, { as: 'CreatedBy' });
  MercadolibreConnectionConfiguration.belongsTo(User, { as: 'UpdatedBy' });

  var MarketplaceConnection = sequelize.import('../marketplace-connection/marketplace-connection.model');
  MercadolibreConnectionConfiguration.belongsTo(MarketplaceConnection);

  MercadolibreConnectionConfiguration.CategoryMappingTypes = {
    AUTOMATIC: 'automatic',
    MANUAL: 'manual'
  }

  return MercadolibreConnectionConfiguration;

}
