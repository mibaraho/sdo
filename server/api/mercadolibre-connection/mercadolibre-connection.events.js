/**
 * MercadolibreConnection model events
 */

'use strict';

import {EventEmitter} from 'events';
var MercadolibreConnection = require('../../sqldb').MercadolibreConnection;
var MercadolibreConnectionEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MercadolibreConnectionEvents.setMaxListeners(0);

// Model events
var events = {
  'afterCreate': 'save',
  'afterUpdate': 'save',
  'afterDestroy': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  MercadolibreConnection.hook(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc, options, done) {
    MercadolibreConnectionEvents.emit(event + ':' + doc._id, doc);
    MercadolibreConnectionEvents.emit(event, doc);
    done(null);
  }
}

export default MercadolibreConnectionEvents;
