'use strict';

var express = require('express');
var controller = require('./mercadolibre-connection.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/m/:id/mercadolibre-connections/p/:page', auth.isAuthenticated(), controller.index);
router.get('/m/:id/mercadolibre-connections', auth.isAuthenticated(), controller.index);
router.get('/mercadolibre-connections/:id', auth.isAuthenticated(), controller.show);
router.put('/mercadolibre-connections/:id/synchronization/start', auth.isAuthenticated(), controller.startSynchronization);
router.put('/mercadolibre-connections/:id', auth.isAuthenticated(), controller.update);
router.patch('/mercadolibre-connections/:id', auth.isAuthenticated(), controller.update);
router.delete('/mercadolibre-connections/:id', auth.isAuthenticated(), controller.destroy);


router.get('/mercadolibre-connections/:id/products/:itemId', auth.isAuthenticated(), controller.getProduct);
router.get('/mercadolibre-connections/:id/products/:itemId/questions', auth.isAuthenticated(), controller.getProductQuestions);
router.post('/mercadolibre-connections/:id/questions/:questionId/answer', auth.isAuthenticated(), controller.postAnswer);
router.get('/mercadolibre-connections/:id/products', auth.isAuthenticated(), controller.getProducts);
router.get('/mercadolibre-connections/:id/user', auth.isAuthenticated(), controller.getUser);

module.exports = router;

