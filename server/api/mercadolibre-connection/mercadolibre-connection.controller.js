/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/mercadolibre-connections              ->  index
 * POST    /api/mercadolibre-connections              ->  create
 * GET     /api/mercadolibre-connections/:id          ->  show
 * PUT     /api/mercadolibre-connections/:id          ->  update
 * DELETE  /api/mercadolibre-connections/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import {MarketplaceConnection} from '../../sqldb';
import * as pagination from '../../utils/pagination'
import config from '../../config/environment';
import * as mercadolibreSyncManager from '../../components/background-tasks/sync-manager/mercadolibre/queue'
var rp = require('request-promise');


var MercadolibreApiPaths = {
  GET_USER_INFORMATION: '/users/me',
  GET_PRODUCTS: '/items',
  AUTH_TOKEN: '/oauth/token'
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      var update = { status: 'deleted', UpdatedById: req.user._id };
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err)
    res.status(statusCode).send(err);
  };
}

// Gets a list of MercadolibreConnections
export function index(req, res) {
  var _where = {
    status: 'created',
    MerchantId: req.params.id,
    provider: 'mercadolibre'
  }
  MarketplaceConnection.count({
     where: _where
   }).then(function(count){
      var _pagination = {
        limit: count
      }
      if(req.params.page){
        _pagination = pagination.paginate(req.params.page, count)
      }
      MarketplaceConnection.findAll({
        where: _where,
        attributes: ['_id', 'name', 'country', 'pictureUrl'],
        order: [
          ['createdAt', 'DESC']
        ],
        offset: _pagination.offset,
        limit: _pagination.limit
      }).then(function(result){
          var resultWrapper = {}
          resultWrapper.entries = result;
          resultWrapper.pagination = _pagination;
          return resultWrapper
        })
    .then(respondWithResult(res))
    .catch(handleError(res));
    });
}


export function startSynchronization(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(setSynchronizationProcessStatusAsSynchronizing(req))
    .then(queueStartSynchronizationTask(req))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

  function setSynchronizationProcessStatusAsSynchronizing(req){
    return function(marketplaceConnection){
      var updates = { synchronizationStatus: MarketplaceConnection.SynchronizationStatus.SYNCHRONIZING, synchronizationProcessStatus: MarketplaceConnection.SynchronizationProcessStatus.STARTED, UpdatedById: req.user._id }
      return marketplaceConnection.updateAttributes(updates)
        .then(function(result){
          return marketplaceConnection;
        })
    }
  }

  function queueStartSynchronizationTask(req){
    return function(marketplaceConnection){
      mercadolibreSyncManager.queueStartSynchronizationProcessTask(_.pick(marketplaceConnection, '_id'), _.pick(req.user, '_id'))
      return marketplaceConnection
    }
  }

// Gets a single MercadolibreConnection from the DB
export function show(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    },
    attributes: ['_id', 'name', 'provider', 'auth_type', 'api_user_id', 'api_user_id']
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets the user associated to the connection
export function getUser(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(performUserRequest(req))
    .then(respondWithResult(res))
    .catch(handleExpiredTokenError(req, res, getUser))
    .catch(handleError(res));
}

// Gets the products associated to the connection
export function getProducts(req, res) {
  return MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(performProductsRequest(res))
    .then(respondWithResult(res))
    .catch(handleExpiredTokenError(req, res, getProducts))
    .catch(handleError(res));
}


// Gets the user associated to the connection
export function getProduct(req, res) {
  return MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(performProductRequest(req))
    .then(respondWithResult(res))
    .catch(handleExpiredTokenError(req, res, getProduct))
    .catch(handleError(res));
}

// Gets the questions associated a product
export function getProductQuestions(req, res) {
  return MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(performProductQuestionsRequest(req))
    .then(respondWithResult(res))
    .catch(handleExpiredTokenError(req, res, getProductQuestions))
    .catch(handleError(res));
}

// Post and answer
export function postAnswer(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(performPostAnswer(req))
    .then(respondWithResult(res))
    .catch(handleExpiredTokenError(req, res, postAnswer))
    .catch(handleError(res));
}

function performUserRequest(req){
  return function(marketplaceConnection){
    var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + MercadolibreApiPaths.GET_USER_INFORMATION,
        qs: { access_token: marketplaceConnection.access_token },
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
  }
}

function performProductsRequest(req){
  return function(marketplaceConnection){
    var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + '/users/'+marketplaceConnection.user_id +'/items/search',
        qs: { access_token: marketplaceConnection.access_token },
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
  }
}

function performProductRequest(req){
  return function(marketplaceConnection){
    var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + '/items/' + req.params.itemId,
        qs: { access_token: marketplaceConnection.access_token },
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
  }
}

function performProductQuestionsRequest(req){
  return function(marketplaceConnection){
    var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
    var options = {
        uri: _config.apiBaseUrl + '/questions/search',
        qs: { item_id: req.params.itemId,access_token: marketplaceConnection.access_token },
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
  }
}
function performPostAnswer(req){
  return function(marketplaceConnection){
    var _body = {
      question_id: req.params.questionId,
      text: req.body.text
    }
    console.log(_body)
    var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
    var options = {
        method: 'POST',
        body: _body,
        uri: _config.apiBaseUrl + '/answers',
        qs: {access_token: marketplaceConnection.access_token},
        json: true // Automatically parses the JSON string in the response
    };
    return rp(options)
  }
}



function handleExpiredTokenError(req, res, fn) {//Hacker AF
  return function(err) {
    if(err.error && err.error.message == 'invalid_token') {
      return MarketplaceConnection.find({
        where: {
          _id: req.params.id
        }
      }).then(function(marketplaceConnection){
        var _config = _.find(config.mercadolibre.countries, function(item){ return item.code.toLowerCase() == marketplaceConnection.country.toLowerCase()})
        var options = {
            method: 'POST',
            body: {},
            uri: _config.apiBaseUrl + MercadolibreApiPaths.AUTH_TOKEN,
            qs: {
              grant_type: 'refresh_token',
              client_id: _config.appId,
              client_secret: _config.secretKey,
              refresh_token: marketplaceConnection.refresh_token
            },
            json: true // Automatically parses the JSON string in the response
        };
        return rp(options).then(function(result){//Once the refresh token is aquired, update the connection tokens and repeat the entire request.
          marketplaceConnection.updateAttributes(result).then(function(result){
            fn(req, res)
          })

        })
      })
    }
    throw err
  };
}


// Updates an existing MercadolibreConnection in the DB
export function update(req, res) {
  var _mercadolibre_connection = _.pick(req.body, 'name')
  _mercadolibre_connection.UpdatedById = req.user._id
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(_mercadolibre_connection))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MercadolibreConnection from the DB
export function destroy(req, res) {
  MarketplaceConnection.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(req, res))
    .catch(handleError(res));
}
