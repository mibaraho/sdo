'use strict';

var app = require('../..');
import request from 'supertest';

var newMercadolibreConnection;

describe('MercadolibreConnection API:', function() {

  describe('GET /api/mercadolibre-connections', function() {
    var mercadolibreConnections;

    beforeEach(function(done) {
      request(app)
        .get('/api/mercadolibre-connections')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mercadolibreConnections = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(mercadolibreConnections).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/mercadolibre-connections', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/mercadolibre-connections')
        .send({
          name: 'New MercadolibreConnection',
          info: 'This is the brand new mercadolibreConnection!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMercadolibreConnection = res.body;
          done();
        });
    });

    it('should respond with the newly created mercadolibreConnection', function() {
      expect(newMercadolibreConnection.name).to.equal('New MercadolibreConnection');
      expect(newMercadolibreConnection.info).to.equal('This is the brand new mercadolibreConnection!!!');
    });

  });

  describe('GET /api/mercadolibre-connections/:id', function() {
    var mercadolibreConnection;

    beforeEach(function(done) {
      request(app)
        .get('/api/mercadolibre-connections/' + newMercadolibreConnection._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mercadolibreConnection = res.body;
          done();
        });
    });

    afterEach(function() {
      mercadolibreConnection = {};
    });

    it('should respond with the requested mercadolibreConnection', function() {
      expect(mercadolibreConnection.name).to.equal('New MercadolibreConnection');
      expect(mercadolibreConnection.info).to.equal('This is the brand new mercadolibreConnection!!!');
    });

  });

  describe('PUT /api/mercadolibre-connections/:id', function() {
    var updatedMercadolibreConnection;

    beforeEach(function(done) {
      request(app)
        .put('/api/mercadolibre-connections/' + newMercadolibreConnection._id)
        .send({
          name: 'Updated MercadolibreConnection',
          info: 'This is the updated mercadolibreConnection!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMercadolibreConnection = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMercadolibreConnection = {};
    });

    it('should respond with the updated mercadolibreConnection', function() {
      expect(updatedMercadolibreConnection.name).to.equal('Updated MercadolibreConnection');
      expect(updatedMercadolibreConnection.info).to.equal('This is the updated mercadolibreConnection!!!');
    });

  });

  describe('DELETE /api/mercadolibre-connections/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/mercadolibre-connections/' + newMercadolibreConnection._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when mercadolibreConnection does not exist', function(done) {
      request(app)
        .delete('/api/mercadolibre-connections/' + newMercadolibreConnection._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
