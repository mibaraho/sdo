'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var mercadolibreConnectionCtrlStub = {
  index: 'mercadolibreConnectionCtrl.index',
  show: 'mercadolibreConnectionCtrl.show',
  create: 'mercadolibreConnectionCtrl.create',
  update: 'mercadolibreConnectionCtrl.update',
  destroy: 'mercadolibreConnectionCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var mercadolibreConnectionIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './mercadolibre-connection.controller': mercadolibreConnectionCtrlStub
});

describe('MercadolibreConnection API Router:', function() {

  it('should return an express router instance', function() {
    expect(mercadolibreConnectionIndex).to.equal(routerStub);
  });

  describe('GET /api/mercadolibre-connections', function() {

    it('should route to mercadolibreConnection.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'mercadolibreConnectionCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/mercadolibre-connections/:id', function() {

    it('should route to mercadolibreConnection.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'mercadolibreConnectionCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/mercadolibre-connections', function() {

    it('should route to mercadolibreConnection.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'mercadolibreConnectionCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/mercadolibre-connections/:id', function() {

    it('should route to mercadolibreConnection.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'mercadolibreConnectionCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/mercadolibre-connections/:id', function() {

    it('should route to mercadolibreConnection.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'mercadolibreConnectionCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/mercadolibre-connections/:id', function() {

    it('should route to mercadolibreConnection.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'mercadolibreConnectionCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
